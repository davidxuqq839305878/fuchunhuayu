<?php
/**
 * User: David Xu
 * Date: 2/14/14
 * Time: 3:38 PM
 */
/*每朵花的信息*/
$flowerArray = array();
$flowerArray[0] = array();//第1朵的信息
$flowerArray[1] = array();//第2朵的信息
$flowerArray[2] = array();//第3朵的信息

$flowerArray[3] = array();//第4朵的信息

$flowerArray[4] = array();//第5朵的信息
$flowerArray[5] = array();//第6朵的信息
$flowerArray[6] = array();//第7朵的信息

$flowerArray[0] = array(
    'id'   => '1',
    'name' => '富贵1',//花语名称
    'des'  => '富贵的花语具有美好的含义1',//花语描述
    'meta' => '备注1',//花语备注
);
$flowerArray[1] = array(
    'id'   => '2',
    'name' => '富贵2',//花语名称
    'des'  => '富贵的花语具有美好的含义2',//花语描述
    'meta' => '备注2',//花语备注
);
$flowerArray[2] = array(
    'id'   => '3',
    'name' => '富贵3',//花语名称
    'des' => '富贵的花语具有美好的含义3',//花语描述
    'meta' => '备注3',//花语备注
);
$flowerArray[3] = array(
    'id'   => '4',
    'name' => '富贵4',//花语名称
    'des' => '富贵的花语具有美好的含义4',//花语描述
    'meta' => '备注4',//花语备注
);
$flowerArray[4] = array(
    'id'   => '5',
    'name' => '富贵5',//花语名称
    'des' => '富贵的花语具有美好的含义5',//花语描述
    'meta' => '备注5',//花语备注
);
$flowerArray[5] = array(
    'id'   => '6',
    'name' => '富贵6',//花语名称
    'des' => '富贵的花语具有美好的含义6',//花语描述
    'meta' => '备注6',//花语备注
);
$flowerArray[6] = array(
    'id'   => '7',
    'name' => '富贵7',//花语名称
    'des' => '富贵的花语具有美好的含义7',//花语描述
    'meta' => '备注7',//花语备注
);
$flowerArray[7] = array(
    'id'   => '8',
    'name' => '富贵8',//花语名称
    'des' => '富贵的花语具有美好的含义8',//花语描述
    'meta' => '备注8',//花语备注
);

