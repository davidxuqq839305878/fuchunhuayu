<?php
/**
 * User: David Xu
 * Date: 2/17/14
 * Time: 3:49 PM
 */
//include './flowerinfo.php';

include "./conn.php";
include "./funs.php";
include "./flowerinfo.php";

$wxid = $_GET['wxid'] ? trim($_GET['wxid']):'opXN8uDV7ZhcGJc-VtSUmQbnkiH4';
$from = $_GET['from'] ? trim($_GET['from']):'self';
$awardcount = getFlowerCount($wxid);
?>
<script src="http://lib.sinaapp.com/js/jquery/1.7.2/jquery.min.js"></script>
<div class="flower" id="flower1"></div>
<div class="flower" id="flower2"></div>
<div class="flower" id="flower3"></div>
<div class="flower" id="flower4"></div>
<div class="flower" id="flower5"></div>
<div class="flower" id="flower6"></div>

<input type="text" id="wxid" name="wxid" value="<?php echo $wxid;?>"/>
<input type="text" id="from" name="from" value="<?php echo $from;?>"/>
<input type="text" id="awardcount" name="awardcount" value="<?php echo $awardcount;?>"/>
<input type="text" id="flowerlimitcount" name="flowerlimitcount" value="3"/>

<script>
    var hostUrl = 'http://chenhao.imagchina.com/fuchunhuayu/game';
    var startI = Math.ceil(Math.random()*6);
    var awardArr = [];
    var wxid = document.getElementById('wxid').value;
    var from = document.getElementById('from').value;
    var awardcount = document.getElementById('awardcount').value;
    var flowerLimitCount = 3;//默认可以采集三朵花语
    var flowerJson = {};//从服务器端获取花语信息
    flowerJson['1'] = {
        id : '1',
        name : '富贵1',
        des : '富贵的花语具有美好的含义1',
        meta :  '备注1'
    };
    flowerJson['2'] = {
        id : '2',
        name : '富贵2',
        des : '富贵的花语具有美好的含义2',
        meta :  '备注2'
    };
    flowerJson['3'] = {
        id : '3',
        name : '富贵3',
        des : '富贵的花语具有美好的含义3',
        meta :  '备注3'
    };
    flowerJson['4'] = {
        id : '4',
        name : '富贵4',
        des : '富贵的花语具有美好的含义4',
        meta :  '备注4'
    };
    flowerJson['5'] = {
        id : '5',
        name : '富贵5',
        des : '富贵的花语具有美好的含义5',
        meta :  '备注5'
    };
    flowerJson['6'] = {
        id : '6',
        name : '富贵6',
        des : '富贵的花语具有美好的含义6',
        meta :  '备注6'
    };
    flowerJson['7'] = {
        id : '7',
        name : '富贵7',
        des : '富贵的花语具有美好的含义7',
        meta :  '备注7'
    };
    flowerJson['8'] = {
        id : '8',
        name : '富贵8',
        des : '富贵的花语具有美好的含义8',
        meta :  '备注8'
    };

    var awardFlowerJson ={};

    function showGame(){
        /*
        *恭喜您，你已收集到三朵花语，剩余花语需要朋友帮忙收集。
        *点击右上角分享到朋友圈，让您的好友帮您一起收集花语吧！
        */
        var from = document.getElementById('from').value;
        var awardcount = document.getElementById('awardcount').value;
        if(from == 'share'){
            alert('帮助你的朋友采集花语吧');
        }
        if(from == 'self'){
            if(document.getElementById('awardcount').value >= 3){
                alert("恭喜您，你已收集到三朵花语，剩余花语需要朋友帮忙收集。点击右上角分享到朋友圈，让您的好友帮您一起收集花语吧！");
            }
        }
    }
    showGame();
    function getFlowerlimit(){
        /*
        * 查看微信用户有机会得到多少朵花语
        * */
        var wxid = document.getElementById('wxid').value;
        $.ajax({
            type: "GET",
            url:hostUrl+"/getawardflower.php?wxid="+wxid+"&from=self-limit",
            success:function(data){
                //alert(data);
                document.getElementById("flowerlimitcount").value = parseInt(data);
            },
            error:function(msg){
                alert(msg);
            }
        });

    }
    getFlowerlimit();
    for(var i = startI; i <= startI + parseInt(document.getElementById("flowerlimitcount").value) -1; i++){
        if(i <= 6 ){
            awardArr.push(i);
        }else{
            awardArr.push(i%6);
        }
    }
    awardArr.sort();
    function updateData(){
        awardcount = parseInt(document.getElementById('awardcount').value);
        $.ajax({
                type: "GET",
                url:hostUrl+"/getawardflower.php?wxid="+wxid+"&from="+from+"&awardcount="+awardcount,
                success:function(data){

                },
                error:function(msg){
                    alert(msg);
                }
            });
    }
    for(var j = 0 ; j<= parseInt(document.getElementById("flowerlimitcount").value) -1; j++){
        document.getElementById("flower"+awardArr[j]).onclick = function(){
            document.getElementById("flowerlimitcount").value = parseInt(document.getElementById("flowerlimitcount").value) - 1;
            $.ajax({
                type: "GET",
                url: hostUrl+"/getawardflower.php?wxid="+wxid+"&from="+from+"&awardcount="+awardcount,
                /*data: {},*/
                contentType: "application/json; charset=utf-8",
                //dataType: "json",
                success: function (data) {
                    // Play with returned data in JSON format
                    if(data=='getaflower'){
                        //少于三朵花语，可以继续获得
                        if(awardcount < 3){
                            awardcount = parseInt(awardcount) + 1;
                            alert(awardcount);
                            document.getElementById('awardcount').value = awardcount;
                            if(awardcount  == 3){
                                //等于三朵花语，提示
                                /*恭喜您，你已收集到三朵花语，剩余花语需要朋友帮忙收集。
                                 点击右上角分享到朋友圈，让您的好友帮您一起收集花语吧！
                                 * */
                                /*
                                 *请求更新数据库                            *
                                 * */
                                updateData();//更新数据库，记录前三朵采集的时间
                                alert('恭喜您，你已收集到三朵花语，剩余花语需要朋友帮忙收集');
                            }
                        }else{
                            alert('恭喜您，你已收集到三朵花语，剩余花语需要朋友帮忙收集');
                        }
                    }else if(data == 'err'){
                            alert('此用户未参加采集花语活动，非法进入此页面');
                    }else if(data =='getaflowerbyshare'){
                        //多于三朵花语，提示
                        /*恭喜您，你已收集到三朵花语，剩余花语需要朋友帮忙收集。
                         点击右上角分享到朋友圈，让您的好友帮您一起收集花语吧！
                         * */
                        document.getElementById('awardcount').value = parseInt(document.getElementById('awardcount').value) + 1;
                        alert('恭喜您帮助你的朋友获得一朵花语');
                    }else if(data =='notgetaflowerbyshareltthree'){
                        alert('您的朋友还未获得三朵花语，还不能通过朋友花朵其他花语');
                    }else{
                         alert(data+"有错误！");
                    }
                },
                error: function (msg) {
                    alert(msg);
                }
            });
            this.style.display = 'none';
        }
    }
    console.log(awardArr);
    //console.log(flowerJson);
</script>
<style>
    .flower{ width:40px; height: 40px; float:left; margin: 10px; background: red;}
</style>