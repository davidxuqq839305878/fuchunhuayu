<?php
/**
 * User: David Xu
 * Date: 2/14/14
 * Time: 6:01 PM
 */

//http://www.elegant-prosper.com/peony/game/
//define('GAMEURL', 'http://www.elegant-prosper.com/peony/game');

date_default_timezone_set('PRC');//设置当前时区为中国

function logdebug($url, $text){
    $time = date("Y-m-d H:i:s",time());
    file_put_contents($url ,$text." ".$time."\n",FILE_APPEND);
}

function insertUserFlowerActivity($wxid, $name, $jointime, $lasttime, $tel, $activityinfo){
    if(existsUserFlowerActivity($wxid,$dateId)){

    }else{
        $sql = "INSERT INTO user_flower_activity (wxid, name, jointime, lasttime, tel, activityinfo) VALUES('$wxid', '$name', '$jointime', '$lasttime', '$tel', '$activityinfo')";
    }
    mysql_query($sql);
}

function existsUserInfo($wxid){
    $sql = "SELECT count(*) as count  FROM user_flower_activity WHERE wxid = '$wxid'";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    if($row['count'] == 0 ){
        return false;//未参加活动
    }else{
        return true;//已经参加活动
    }
}


function existsUserFlowerActivity($wxid, $dateid){
    $sql = "SELECT count(*) as count  FROM activity_info WHERE wxid = '$wxid' and dateid = '$dateid'";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    if($row['count'] == 0 ){
        return false;//未参加活动
    }else{
        return true;//已经参加活动
    }
}

function countUserFlowerActivity($wxid, $dateid){
    $sql = "SELECT count(*) as count  FROM activity_info WHERE wxid = '$wxid' and dateid = '$dateid'";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    return $row['count'];
}




function getInfoUserFlowerActivity($wxid, $dateid){
   $sql = "SELECT activityarr FROM activity_info WHERE wxid = '$wxid' and dateid = '$dateid'";
   $result = mysql_query($sql);
   $row = mysql_fetch_array($result);
   if(!empty($row['activityarr'])){
        return unserialize($row['activityarr']);
    }else{
        return 0;
    }
}


function updateThreeUserFlowerActivity($wxid, $dateid){
    //三朵花语
    $time = date("Y-m-d H:i:s",time());
    $arr = array();
    $arr[0] = array(
        'time' => $time
    );
    $arr[1] = array(
        'time' => $time
    );
    $arr[2] = array(
        'time' => $time
    );
    $str = serialize($arr);
    $sql = "UPDATE activity_info SET activityarr = '$str' WHERE wxid = '$wxid' and dateid = '$dateid'";
    mysql_query($sql);

}

function updateGtThreeUserFlowerActivity($wxid, $dateid){
    //大于三朵花语
    $time = date("Y-m-d H:i:s",time());
    $sql = "SELECT activityarr FROM activity_info WHERE wxid = '$wxid' and dateid = '$dateid' ";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    $arr = unserialize($row['activityarr']);
    $gtThreeArr = array();
    $gtThreeArr[0] = array(
        'time' => $time
    );
    $newArr = array_merge($arr,$gtThreeArr);
    $newArrStr = serialize($newArr);
    $sql = "UPDATE activity_info SET activityarr = '$newArrStr' WHERE wxid = '$wxid' and dateid = '$dateid'";
    mysql_query($sql);
    return true;
}

function getFlowerCount($wxid, $currentDateId){
    $sql = "SELECT activityarr FROM activity_info WHERE wxid = '$wxid' and dateid = '$currentDateId'";
    $result = mysql_query($sql);
    if($result == false){
        return 0;
    }else{
        $row = mysql_fetch_array($result);
        $arr = @unserialize($row['activityarr']);
        if(empty($arr)){
            return 0;
        }else{
            return count($arr);
        }
    }
}

function getUserTel($wxid){
    $sql = "SELECT tel FROM user_flower_activity WHERE wxid = '$wxid' ";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    return $row['tel'];
}

function getAwardFlowerCount($wxid, $dateid){
    //获取某个微信用户在分享的情况下，也就是在别人点击分享链接的时刻，能允许得到多少朵花
    $time = date("Y-m-d H:i:s", time());
    //count 取值范围1~5
    $userFlowerLimit = getUserFlowerLimit($wxid, $dateid); //当前能获得到第8朵
    $userFlowerCount = getFlowerCount($wxid, $dateid);//当前用户已经获得多少朵
    if($userFlowerLimit < $userFlowerCount){
        return 0;//当前无法获得花语
    }else if($userFlowerLimit == $userFlowerCount){
        return 0;//当前无法获得花语
    }else{
        return $userFlowerLimit - $userFlowerCount;//当前能获得多少朵花语
    }
}
function updateFlowerCost($dateid){
    $sql = "SELECT * FROM `activity_info` WHERE dateid = '$dateid'";
    $result = mysql_query($sql);
    $out = array(
        '0' => 0,
        '3' => 0,
        '4' => 0,
        '5' => 0,
        '6' => 0,
        '7' => 0,
        '8' => 0,
        'err' => 0
    );

    while ($row = mysql_fetch_array($result)){

        $arr = unserialize($row['activityarr']);//获取活动情况

        $flowercount = count($arr);//用户的花语数量


        if($flowercount == 0 || $flowercount == 1){
            $out[0]++;
        }else if($flowercount == 3){
            $out[3]++;
        }else if($flowercount == 4){
            $out[4]++;
        }else if($flowercount == 5){
            $out[5]++;
        }else if($flowercount == 6){
            $out[6]++;
        }else if($flowercount == 7){
            $out[7]++;
        }else if($flowercount == 8){
            $out[8]++;
        }else{
            $out[err]++;
        }
    }
    //print_r($out[7]);
    $updatesql = "UPDATE time_limit SET cost = '$out[4]' WHERE level = 4 and dateid ='$dateid';";
    $updatesql .= "UPDATE time_limit SET cost = '$out[5]' WHERE level = 5 and dateid ='$dateid';";
    $updatesql .= "UPDATE time_limit SET cost = '$out[6]' WHERE level = 6 and dateid ='$dateid';";
    $updatesql .= "UPDATE time_limit SET cost = '$out[7]' WHERE level = 7 and dateid ='$dateid';";
    $updatesql .= "UPDATE time_limit SET cost = '$out[8]' WHERE level = 8 and dateid ='$dateid'";
    $query_e = explode(';',$updatesql);
    foreach ($query_e as $k =>$v)
    {
        //echo $query_e[$k];
        mysql_query($query_e[$k]);
    }

}
function getUserFlowerLimit($wxid, $dateid){

    updateFlowerCost($dateid);
    $time = date("Y-m-d H:i:s", time());
    $timestr = strtotime($time);


    $sql = 'SELECT * FROM time_limit WHERE dateid = "$dateid"';
    $result = mysql_query($sql);
    $limitlevel = array();
    while($row = mysql_fetch_array($result)){
        $starttime = strtotime($row['time_start']);
        $endtime = strtotime($row['time_end']);
        if(($timestr >= $starttime)&&($timestr <= $endtime )&&$row['num']>$row['cost']){
            $limitlevel[] = $row['level'];
        }
    }


    //未写完
    sort($limitlevel);

    //return $limitlevel;

    if(count($limitlevel)>=1){
        return $limitlevel[count($limitlevel)-1];
    }else{
         return 0;
      // return $row;

    }
}

function array_to_json( $array ){
    if( !is_array( $array ) ){
        return false;
    }

    $associative = count( array_diff( array_keys($array), array_keys( array_keys( $array )) ));
    if( $associative ){

        $construct = array();
        foreach( $array as $key => $value ){

            // We first copy each key/value pair into a staging array,
            // formatting each key and value properly as we go.

            // Format the key:
            if( is_numeric($key) ){
                $key = "key_$key";
            }
            $key = "'".addslashes($key)."'";

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = "'".addslashes($value)."'";
            }

            // Add to staging array:
            $construct[] = "$key: $value";
        }

        // Then we collapse the staging array into the JSON form:
        $result = "{ " . implode( ", ", $construct ) . " }";

    } else { // If the array is a vector (not associative):

        $construct = array();
        foreach( $array as $value ){

            // Format the value:
            if( is_array( $value )){
                $value = array_to_json( $value );
            } else if( !is_numeric( $value ) || is_string( $value ) ){
                $value = "'".addslashes($value)."'";
            }

            // Add to staging array:
            $construct[] = $value;
        }

        // Then we collapse the staging array into the JSON form:
        $result = "[ " . implode( ", ", $construct ) . " ]";
    }

    return $result;
}

function getFlowerLimit($wxid, $from, $trueFrom, $dateid){

    if($from == "self-limit"){
        //$trueFrom 链接的真实来源 share 或者 self
        if(existsUserFlowerActivity($wxid, $dateid)){
            $limitFlowerCount = getAwardFlowerCount($wxid, $dateid);//用户此刻能获取多少朵花语，在允许的范围内
            $ownFlowerCount = getFlowerCount($wxid, $dateid);//用户此时拥有多少朵花语
            if($trueFrom == 'self'){

                if($ownFlowerCount == 0){
                    return 3;
                }else if($ownFlowerCount >= 3){
                    return 0;
                }else{

                }

            }else if($trueFrom == 'share'){

                if($ownFlowerCount == 0){
                    return 0;
                }else if($ownFlowerCount >= 3){
                    return $limitFlowerCount;
                }else{

                }

            }else{

            }
        }else{
            return  "err";//此用户未参加采集花语活动，非法进入此页面
        }
    }

}
function getCurrentDateId(){

    //    2014年3月10日-2014年4月6日
    //    第1期花语收集：3月10日-3月23日
    //    第2期花语收集：3月24日-4月6日
    $nowDateStr = date('Y-m-d H:i:s',time());
    $nowTrueDate = strtotime($nowDateStr); //当前时间戳

    $timestr_0 = '2014-03-09 23:59:59';
    $time_0 = strtotime($timestr_0);

    $timestr_1_start = '2014-03-10 00:00:00';
    $timestr_1_end = '2014-03-23 23:59:59';

    $time_1_start = strtotime($timestr_1_start);
    $time_1_end = strtotime($timestr_1_end);


    $timestr_2_start = '2014-03-24 00:00:00';
    $timestr_2_end = '2014-04-06 23:59:59';

    $time_2_start = strtotime($timestr_2_start);
    $time_2_end = strtotime($timestr_2_end);

    $timestr_3 = '2014-04-07 00:00:00';


    if($nowTrueDate < $time_0){
        $dateid = 0;
    }else if($nowTrueDate < $time_1_end){
        $dateid = 1;
    }else if($nowTrueDate < $time_2_end){
        $dateid = 2;
    }else{
        $dateid = 3;
    }
    return $dateid;
}
function getWeiUserJson($weixin_openid){
    if(!empty($_GET[weixin_openid])){
        $weixin_openid = $_GET[weixin_openid];
        $json = file_get_contents('http://why.elegant-prosper.com/?ep_api=user_data&weixin_openid='.$weixin_openid);
        $arr = json_decode($json, true);//json --> arr
        return $arr;
    }
}


