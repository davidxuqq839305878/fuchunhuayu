<?php
/**
 * User: David Xu
 * Date: 3/5/14
 * Time: 12:07 PM
 */
header('Content-type: application/json;charset=UTF-8');
$wxid = $_POST['weixin_openid'];
$name = $_POST['user_name'];
$tel = $_POST['user_phone'];
$url = 'http://why.elegant-prosper.com/?ep_api=user_reg';
$post_data = array(
    'weixin_openid' => $wxid,
    'user_name' => $name,
    'user_phone' => $tel
);
$content = http_build_query($post_data);
$options = array(
    'http' => array(
        'method' => 'POST',
        'header' => 'Content-type:application/x-www-form-urlencoded',
        'content' => $content,
        'timeout' => 15 * 60 // 超时时间（单位:s）
    )
);

$context = stream_context_create($options);
$result = file_get_contents($url, false, $context);

echo $result;