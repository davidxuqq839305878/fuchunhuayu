  <?php
     include('Net/SSH2.php');

     //ssh 登录帐号
     $user          = 'imag';

     //ssh 登录密码
     $pass          = '113232d';

     //ssh 登录的目标主机, 如果登录失败, 检查服务器ssh配置文件 /etc/sshd_config , 增加localhost的访问权限
     $host          = 'localhost';

     //git项目主目录
     $rootDir       = dirname(dirname(__FILE__));

     //git pull 对应本地分支
     $branch        = 'master';

     //git pull 默认远程分支
     $remote        = 'origin';

     $now           = date("Y-m-d H:i:s");

     $ssh = new Net_SSH2($host);
     if (!$ssh->login($user, $pass)) {
         logfile($now.": ssh login failed \n");
         exit();
     }

     //丢弃最后一次pull之后的所有更改..
     $ssh->exec('cd '.$rootDir.'; git reset --hard HEAD');
     logfile($now.": 丢弃最后一次pull之后的所有更改 \n");
     
     //开始同步
     $ssh->exec('cd '.$rootDir.'; git pull '.$remote.' '.$branch);
     logfile($now.": 更新本地分支成功 \n");
     
     function logfile($text){
        file_put_contents(dirname(__FILE__).'/autopull.log', $text, FILE_APPEND);
     }
  ?>