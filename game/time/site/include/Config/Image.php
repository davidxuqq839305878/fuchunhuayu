<?php
class Config_Image{
	public static function getImage($id){
		$list = array("big"=>array("","png"),"small"=>array("s","jpg"),"list"=>array());
		if($id==1){
			for($i=1;$i<=15;$i++){
				$j = $i;
				if($i<10){
					$j = "0".$i;
				}
				$list["list"][] = $j;
			}
		}elseif($id==2){
			$list["big"]   = array("yx","png");
			$list["small"] = array("yx_","jpg");
			for($i=1;$i<=48;$i++){
				$j = $i;
				if($i<10){
					$j = "0".$i;
				}
				$list["list"][] = $j;
			}
		}
		return $list;
	}

    /**
     * 得到合成图像的坐标点
     */
    public static function getFusePostion(){
        $imagePos = array();
        $imagePos['embrace'][6]['x'] = 321;
        $imagePos['embrace'][6]['y'] = 354;
        $imagePos['embrace'][7]['x'] = 321;
        $imagePos['embrace'][7]['y'] = 328;
        $imagePos['embrace'][8]['x'] = 330;
        $imagePos['embrace'][8]['y'] = 284;
        $imagePos['embrace'][9]['x'] = 329;
        $imagePos['embrace'][9]['y'] = 283;
        $imagePos['embrace'][10]['x'] = 329;
        $imagePos['embrace'][10]['y'] = 281;
        //$imagePos['embrace'][11]['x'] = 329;
        //$imagePos['embrace'][11]['y'] = 281;
        $imagePos['embrace'][12]['x'] = 329;
        $imagePos['embrace'][12]['y'] = 279;
        $imagePos['embrace'][13]['x'] = 329;
        $imagePos['embrace'][13]['y'] = 273;
        $imagePos['embrace'][14]['x'] = 329;
        $imagePos['embrace'][14]['y'] = 271;
        $imagePos['embrace'][15]['x'] = 329;
        $imagePos['embrace'][15]['y'] = 271;
        $imagePos['embrace'][16]['x'] = 329;
        $imagePos['embrace'][16]['y'] = 271;
        $imagePos['embrace'][17]['x'] = 329;
        $imagePos['embrace'][17]['y'] = 271;
        $imagePos['embrace'][18]['x'] = 329;
        $imagePos['embrace'][18]['y'] = 271;
        $imagePos['embrace'][19]['x'] = 329;
        $imagePos['embrace'][19]['y'] = 271;
        $imagePos['embrace'][20]['x'] = 329;
        $imagePos['embrace'][20]['y'] = 271;
        
        $imagePos['save'][10]['date']['x'] = 180;
        $imagePos['save'][10]['date']['y'] = 186;
        $imagePos['save'][10]['desc']['x'] = 241;
        $imagePos['save'][10]['desc']['y'] = 186;
        $imagePos['save'][10]['overage']['x'] = 347;
        $imagePos['save'][10]['overage']['y'] = 186;
        $imagePos['save'][10]['sign']['x'] = 362;
        $imagePos['save'][10]['sign']['y'] = 270;        
                        
        $imagePos['save'][11]['date']['x'] = 197;
        $imagePos['save'][11]['date']['y'] = 213;
        $imagePos['save'][11]['desc']['x'] = 255;
        $imagePos['save'][11]['desc']['y'] = 213;
        $imagePos['save'][11]['overage']['x'] = 356;
        $imagePos['save'][11]['overage']['y'] = 213;
        $imagePos['save'][11]['sign']['x'] = 371;
        $imagePos['save'][11]['sign']['y'] = 300;           
        
        $imagePos['save'][12]['date']['x'] = 200;
        $imagePos['save'][12]['date']['y'] = 235;
        $imagePos['save'][12]['desc']['x'] = 257;
        $imagePos['save'][12]['desc']['y'] = 235;
        $imagePos['save'][12]['overage']['x'] = 355;
        $imagePos['save'][12]['overage']['y'] = 235;  
        $imagePos['save'][12]['sign']['x'] = 370;
        $imagePos['save'][12]['sign']['y'] = 320;           
        
        $imagePos['save'][13]['date']['x'] = 200;
        $imagePos['save'][13]['date']['y'] = 235;
        $imagePos['save'][13]['desc']['x'] = 257;
        $imagePos['save'][13]['desc']['y'] = 235;
        $imagePos['save'][13]['overage']['x'] = 355;
        $imagePos['save'][13]['overage']['y'] = 235; 
        $imagePos['save'][13]['sign']['x'] = 370;
        $imagePos['save'][13]['sign']['y'] = 320;            
        
        $imagePos['save'][14]['date']['x'] = 200;
        $imagePos['save'][14]['date']['y'] = 235;
        $imagePos['save'][14]['desc']['x'] = 257;
        $imagePos['save'][14]['desc']['y'] = 235;
        $imagePos['save'][14]['overage']['x'] = 355;
        $imagePos['save'][14]['overage']['y'] = 235;  
        $imagePos['save'][14]['sign']['x'] = 370;
        $imagePos['save'][14]['sign']['y'] = 320;                 
        
        $imagePos['save'][15]['date']['x'] = 200;
        $imagePos['save'][15]['date']['y'] = 235;
        $imagePos['save'][15]['desc']['x'] = 257;
        $imagePos['save'][15]['desc']['y'] = 235;
        $imagePos['save'][15]['overage']['x'] = 355;
        $imagePos['save'][15]['overage']['y'] = 235;   
        $imagePos['save'][15]['sign']['x'] = 370;
        $imagePos['save'][15]['sign']['y'] = 320;

        $imagePos['save']['city']['x'] = 498;
        $imagePos['save']['city']['y'] = 68;

        $imagePos['save']['month']['x'] = 478;
        $imagePos['save']['month']['y'] = 38;

        $imagePos['save']['temp']['x'] = 550;
        $imagePos['save']['temp']['y'] = 70;


        $imagePos['reunion'][17]['x'] = 304;
        $imagePos['reunion'][17]['y'] = 164;
        $imagePos['reunion'][18]['x'] = 304;
        $imagePos['reunion'][18]['y'] = 164;

        return $imagePos;
    }

}
?>