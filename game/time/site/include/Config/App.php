<?php
define("CACHE_LEFTTIME",Config_App::$cache_lefttime);

class Config_App
{
	public static $cache_lefttime = 3600;
    public static $game_host_url = 'http://www.elegant-prosper.com/peony/game';//David.Xu
    //http://chenhao.imagchina.com/fuchunhuayu/game
	/**
	 * Current script scope directory root
	 */
	public static function basedir(){
		if(defined('BASEDIR')){
			return BASEDIR;
		}
		return dirname(dirname(__FILE__));

	}

	/**
	 * Root directory of this site
	 */
	public static function homedir(){
		if(defined('HOMEDIR')){
			return HOMEDIR;
		}
		return dirname(dirname(dirname(dirname(__FILE__))));
	}

	/**
	 * Root directory of this site
	 */
	public static function rootdir(){
		return dirname(dirname(dirname(dirname(__FILE__))));
	}

	/**
	 * Root web root of this site
	 */
	public static function homeurl(){
		return "http://".self::domain();
	}

	/**
	 * current domain
	 */
	public static function domain(){
		return $_SERVER['HTTP_HOST'];
	}

	public static function rootDomain(){
		return $_SERVER['HTTP_HOST'];
	}

	public static function payDomain(){
		return $_SERVER['HTTP_HOST'];
	}

/**---------------------Cookie config----------------------------*/
	/**
	 * @return cookie domain
	 */
	public static function getCookieDomain()
	{
		return $_SERVER['HTTP_HOST'];
	}

	/**
	 * @return cookie expires
	 */
	public static function getCookieExpires()
	{
		return 0;
	}

	/**
	 * @return cookie path
	 */
	public static function getCookiePath()
	{
		return '/';
	}

	/**
	 * @return cookie secure
	 */
	public static function getCookieSecure()
	{
		return false;
	}

	/**
	 * @return cookie key
	 */
	public static function getCookieKey()
	{
		return "apllpsdsmmwqewqewqwennhyr!!@@#";
	}

	public static function formhash($specialadd='',$timestamp=''){
		$user_id  = Fuse_Cookie::getInstance()->user_id;
		$nickname = Fuse_Cookie::getInstance()->nickname;
		$hashadd  = 'qn2';
		$sitekey  = self::getCookieKey();
		if(empty($timestamp)){
			$timestamp = time();
		}
		return substr(md5(substr($timestamp, 0, -7).$nickname.$user_id.$sitekey.$hashadd.$specialadd), 8, 10);
	}

	/**
	* @return real ip
	*/
	public static function getIP()
	{
		global $ip;
		if (getenv("HTTP_CLIENT_IP"))
		$ip = getenv("HTTP_CLIENT_IP");
		else if(getenv("HTTP_X_FORWARDED_FOR"))
		$ip = getenv("HTTP_X_FORWARDED_FOR");
		else if(getenv("REMOTE_ADDR"))
		$ip = getenv("REMOTE_ADDR");
		else $ip = "Unknow";
		return $ip;
	}

	public static function getPrefixImage($image,$prefix)
	{
		$filelist = explode(".",$image);
		if(count($filelist)<2){
			return $image;
		}
		$index = count($filelist)-2;
		$filelist[$index] .= "_".$prefix;
		$filelist[count($filelist)-1] = "jpg";
		$pathname = implode(".",$filelist);
		return $pathname;

	}

}
?>
