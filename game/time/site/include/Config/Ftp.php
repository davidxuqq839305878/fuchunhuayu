<?php
/**
 * @category   Fuse
 * @package    Config
 * @author     David Xu
 */
class Config_Ftp
{
	public static $ftproot = "";

	public static $ftplist = array(
		array(
			"ip"       => "127.0.0.1",
			"port"     => 21,
			"username" => "",
			"password" => ""
		)
	);
}
?>
