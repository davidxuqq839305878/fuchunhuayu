<?php
session_start();
include_once(dirname(dirname(__FILE__)).'/include/weibo2config.php' );
include_once(dirname(dirname(__FILE__)).'/library/sina/saetv2.ex.class.php' );

class Sina
{
    function __construct(){}

    public function request($callback=""){

		$o = new SaeTOAuthV2( WB_AKEY , WB_SKEY );
		if(empty($callback)){
			$callback = WB_CALLBACK_URL;
		}
		$code_url = $o->getAuthorizeURL( WB_CALLBACK_URL );
	    header("Location: $code_url");

    }


    public function callback($method=array()){

		$o = new SaeTOAuthV2( WB_AKEY , WB_SKEY );

		if (isset($_REQUEST['code'])) {
			$keys = array();
			$keys['code'] = $_REQUEST['code'];
			$keys['redirect_uri'] = WB_CALLBACK_URL;
			try {
				$token = $o->getAccessToken( 'code', $keys ) ;
			} catch (OAuthException $e) {
			}
		}

		if ($token) {
			$_SESSION['token'] = $token;
			setcookie( 'weibojs_'.$o->client_id, http_build_query($token) );
			$w = new SaeTClientV2( WB_AKEY , WB_SKEY , $_SESSION['token']['access_token'] );
			$msg = $w->get_uid();
			$_SESSION['last_key']['user_id'] = $msg['uid'];
			call_user_func_array($method,array($w,$_SESSION['last_key']['user_id']));
		}

    }

    public function getClient(){

		if(!isset($_SESSION['token']['access_token']) ){
            return null;
        }

		$w = new SaeTClientV2( WB_AKEY , WB_SKEY , $_SESSION['token']['access_token'] );

        return $w;

    }

	public function getWeiboId(){
		if(!isset($_SESSION['last_key']) || !isset($_SESSION['last_key']['user_id'])){
			return null;
		}
		return $_SESSION['last_key']['user_id'];
	}

}
?>