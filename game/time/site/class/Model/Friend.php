<?php
/**
 * Example model
 * Created 2012-11-25 11:27:51
 * @package		classes
 * @subpackage	operator.weibo_friends
 * @author gary wang (wangbaogang123@hotmail.com)
 *
 */
class Model_Friend extends Fuse_Model
{

	/**
	 * array
	 */
	private $table = array("name"=>"weibo_friends","key"=>"weibo_friend_id");

	public function __construct($config=array())
	{
		parent::__construct($config);
	}

	public function getList($start=0,$per_page=10,$where=1, $platform="")
	{
		$list = array();
		$sql = "SELECT `weibo_uid`,`screen_name`, `head` FROM `".$this->table['name']."` WHERE {$where} ORDER BY rand() DESC";

		if($start>=0 && !empty($per_page)){
			$sql .= " LIMIT {$start},{$per_page}";
		}

		//var_dump($sql);

		if( ($stmt = $this->db->query($sql)) )
		{
			while ( $row = $stmt->fetch() )
			{	
				$row["avatar"] = "http://tp4.sinaimg.cn/".$row["weibo_uid"]."/50/1254308176";
 				$list[] = $row;
			}
		}
		return $list;
	}
    
	public function getTotal($where=1)
	{
		$total = 0;

		$sql = "SELECT COUNT(*) AS total FROM `".$this->table['name']."` WHERE {$where}";
		//var_dump($sql);

		if(($stmt = $this->db->query($sql)))
		{
			if($row = $stmt->fetch())
			{
				$total = $row['total'];
			}
		}

		return $total;
	}

	function getKey()
	{
		return $this->table['key'];
	}

	function getTable()
	{
		return $this->table['name'];
	}
}
?>
