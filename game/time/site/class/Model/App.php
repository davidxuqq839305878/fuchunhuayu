<?php
/**
 * Example model
 * Created 2012-11-25 11:24:42
 * @package		classes
 * @subpackage	operator.game
 * @author gary wang (wangbaogang123@hotmail.com)
 * 
 */

class Model_App extends Fuse_Model
{

	public function __construct($config=array())
	{
		parent::__construct($config);
	}

	public function updates($sql){
		$this->db->query($sql);
	}


}
?>