<?php
include_once(Config_App::homedir()."/site/plugin/sns/class/Sina.php");
 /**
 * @package     class
 * @author gary wang (wangbaogang123@hotmail.com)
 */
class Wei2Controller extends Fuse_Controller
{
    private $config = null;
    /**
     * Constructor
     */
   public function __construct($config = array())
    {
        parent::__construct($config);
        $this->registerTask( 'request','request');
        $this->registerTask( 'callback','callback');
        $this->registerTask( 'check','check');
        $this->registerTask( 'logout','logout');
		$this->registerTask( 'cancel','cancel');
		$this->registerTask( 'share','share');
    }

	public function logout(){
		// $forward = Fuse_Request::getForward("forward");
		// if(empty($forward)){
		// 	$forward = "/";
		// }
		$forward = "/";

		$this->cancel();
		header("Location: {$forward}");
	}

	public function cancel(){
		$sina   = new Sina();
        $weiboClient = $sina->getClient();
		if(!empty($weiboClient)){
			$weiboClient->end_session();
		}
		unset($_SESSION['last_key']['user_id']);
		unset($_SESSION['token']['access_token']);
		unset($_SESSION['last_key']);
		unset($_SESSION['token']);
		unset($_SESSION['wuid']);
		unset($_SESSION['uid']);
		unset($_SESSION['screen_name']);
		unset($_SESSION['platform']);
	}

	public function check(){
		$sina   = new Sina();
        $weiboClient = $sina->getClient();
		$weibouid = $sina->getWeiboId();

        if(empty($weiboClient)||empty($weibouid)||!isset($_SESSION["screen_name"])){
			echo json_encode(array("result"=>"WEIBO_NO_LOGIN"));
        }else{
			$user = array();
			$user["weibo_uid"]   = $weibouid;
			$user["screen_name"] = $_SESSION["screen_name"];
			echo json_encode(array("result"=>"OK","user"=>$user));
		}
	}

   public function request(){
        $sina   = new Sina();
        $weiboClient = $sina->getClient();
        
        $t = Fuse_Request::getVar("t",'get');
        
   		$forward = Fuse_Request::getForward("forward");

   		if(!empty($_SESSION["t"])){

   		}elseif(!empty($t)){
			$_SESSION["t"] = $t;
		}elseif(!empty($forward)){
			$_SESSION["t"] = $forward;
		}else{
			$_SESSION["t"] = '';
		}
		
        if(!empty($weiboClient)){
            $this->sina_callback($weiboClient,$sina->getWeiboId());
            exit();
        }
		
		$callback = Config_App::homeurl()."/weibo2/callback.php";
        $sina->request($callback);
    }

   public function callback(){
   		$error = Fuse_Request::getVar("error_uri",'get');
   		if(!empty($error)){
   			echo "<script>top.window.location.href='/'</script>";
			exit;
   		}

        $sina   = new Sina();
        $sina->callback(array($this,"sina_callback"));
    }
	
   /**
    * 通过地址获取信息
    * @param unknown_type $url
    */
   public function curlget($url, $timeout=5){
   		$ch=curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
		$result=curl_exec($ch);
		curl_close($ch);
		return $result;
   } 
   
   
   public function sina_callback($w,$weibouid=0){
		$t = Fuse_Request::getVar("t",'get');
        $user = $w->show_user_by_id($weibouid);
        if (isset($user["error"])){
            echo "<script>alert('读取微博信息错误！');window.location.href='/';</script>";
			exit();
        }


		$model = new Fuse_Model();
 		$weibouid = $user["id"];//登陆微博者的微博ID

		$checkuser = $model->getRow("SELECT * FROM `weibo_users` WHERE `weibo_uid`=?",array($weibouid));//查看该登陆者过去是否有过登陆记录			

		$object = array();
		$object["weibo_uid"]   = $user["id"];
		$object["screen_name"] = $user["screen_name"];
		$object["gender"]      = $user["gender"];
		$object["province"]    = $user["province"];

		$object["city"]        = $user["city"];
		$object["location"]    = $user["location"];

		$object["photo"]  = preg_replace("/\/50\//i", "/180/", $user['profile_image_url']);

		$object['profile_url'] = "weibo.com/".$user['profile_url'];

		$object['access_token']     = $_SESSION['token']['access_token'];
		$object["ip"]               = $_SERVER["REMOTE_ADDR"];	
		$object['platform']    = 'sina';
		if(empty($checkuser)){
			$from_cookie = Fuse_Cookie::getInstance()->from_cookie;
			if($from_cookie){
				$object["from"]  = $from_cookie;
				Fuse_Cookie::getInstance()->clear("from_cookie");
			}
			$object["ip"]               = $_SERVER["REMOTE_ADDR"];
			$object["time"]             = date("Y-m-d H:i:s");
			$uid = $model->store("weibo_users",$object);//如果没有登陆过则将该用户微博信息写入weibo_users表中
		}else{
			
			$model->update("weibo_users", $object, " `weibo_uid`='{$weibouid}' ");//如果之前登陆过则更新weibo_users数据表
			$user = $checkuser;
			$uid = $user['uid'];
		}
		
		//获取当前登录者的地址
		$user_province = "";
		$user_city = "";
       $ip = $_SERVER['REMOTE_ADDR'];

		
		$list = array();

		$friendlist = $w->bilateral($w,1,200);
		
		if(isset($friendlist["users"])){
			$list = array_merge($list,$friendlist["users"]);
		}

		//获取用户的朋友列表
		foreach($list as $k=>$frienduser){
			$id = $frienduser['id'];
			$row = $model->getRow("SELECT COUNT(*) AS `total` FROM `weibo_friends` WHERE `uid`=$weibouid AND `weibo_uid`=$id");//用问号占位符的方法在本机环境下存在问题固不予以采用
			//一一判断原来是否添加过，如果没有添加过则加入数据表中
			if($row["total"] == 0){
				$objectFriend                = array();
				$objectFriend["uid"]         = $weibouid;
				$objectFriend["weibo_uid"]   = $id;
				$objectFriend["screen_name"] = $frienduser['screen_name'];
				$objectFriend["avatar_hd"] = $frienduser['avatar_hd'];//获取头像信息地址
				$objectFriend["profile_url"] = "weibo.com/".$frienduser['profile_url'];//获取好友的个人主页地址
				$objectFriend["ip"]   = $_SERVER['REMOTE_ADDR'];
				$objectFriend["time"] = date("Y-m-d H:i:s",time());
				$objectFriend['platform'] = "sina";
				$model->store("weibo_friends",$objectFriend);
			}
		}

		
		$_SESSION["wuid"] = $weibouid;
		$_SESSION["screen_name"] = $user['screen_name'];
		$_SESSION["platform"] = "sina";
		$_SESSION["uid"] = $uid;
		$t = $_SESSION["t"];
		if(empty($t)){					
			echo "<script>window.location.href='http://villa.imagchina.com:8003';</script>";
		}else{
			$t = urldecode($t);
			echo "<script>";
			echo "location = '".$t."'";
			echo "</script>";
		}
		
		
    }


	private function getFrientList($weiboClient,$uid,$stat=0){
		$list = "";
		if(isset($weiboClient)){
			$friendsarray = $weiboClient->friends_by_id($uid,$stat, 200);
			if($friendsarray){
				foreach($friendsarray['users'] as $k=>$friend){
					$temp = array();
					$temp['id'] = $friend['id'];
					$temp['screen_name'] = $friend['name'];
					$list[] = $temp;
				}
			}
		}
		return $list;
	}
	
	/**
	 * 合成gif
	 */
	function domakeGif($weibo_uid, $category){
		//图片数
		$picnum = 15;
		$savepath = "save";
		if($category == 1){
			$savepath = "save"; 
		}elseif($category == 2){
			$picnum = 20;
			$savepath = "embrace"; 
		}elseif($category == 3){
			$picnum = 18;
			$savepath = "reunion"; 
		}
		
		$imagelist = array();
		
		//盼团圆生成月饼的图片控制每帧的播放速度
		if($category == 3){
			for($i=1; $i<=$picnum; $i++){
				if($i == $picnum){
					$imagelist[] = ' -delay 500 '.Config_App::homedir()."/upload/".$weibo_uid."/".$savepath."/".$i.".jpg";	
				}else{
					$imagelist[] = ' -delay 20 '.Config_App::homedir()."/upload/".$weibo_uid."/".$savepath."/".$i.".jpg";	
				}
			}
		}else{
			for($i=1; $i<=$picnum; $i++){
				$imagelist[] = Config_App::homedir()."/upload/".$weibo_uid."/".$savepath."/".$i.".jpg";	
			}
		}
		
		
		include_once(dirname(dirname((__FILE__)))."/ImageHelper.php");
		
    	//生成gif
    	$helper = new ImageHelper();
    	$imgopt = array(
    		"src"=>$imagelist,
    		"dest"=>Config_App::homedir()."/combinimage/".$weibo_uid."_".$category.".gif",
    		"delay"=>20,
    		"isLoop"=>0
    	);
    	
    	
    	
    	if($category == 3){
    		$mg = $helper->makeGifReunion($imgopt);
    	}else{
    		$mg = $helper->makeGif($imgopt);
    	}
    	
    	if($mg){
    		return $weibo_uid."_".$category.".gif";
    	}
	} 
	public function share(){
	
    	$sina   = new Sina();
    	$weiboClient = $sina->getClient();
		$weibo_uid = $sina->getWeiboId();
		
    	if(empty($weiboClient)){
    		echo json_encode(array("result"=>"WEIBO_NO_LOGIN"));
    		exit();
    	}

    	if(!isset($_SESSION["uid"])){
			echo json_encode(array("result"=>"WEIBO_NO_LOGIN"));
    		exit();
		}

		if(empty($_SESSION["uid"])){
			echo json_encode(array("result"=>"WEIBO_NO_LOGIN"));
    		exit();
		}

		$uid = $_SESSION["uid"];
		$username = $_SESSION['screen_name'];
    	
    	$category = Fuse_Request::getVar('category','post');
    	$type   = Fuse_Request::getVar('type','post');
    	
    	if(empty($category) || empty($type)){
    		echo json_encode(array("result"=>"INVALID_INPUT"));
    		exit();
    	}
    	
		$friend_1 = Fuse_Request::getVar('friend_1','post');
		$friend_2 = Fuse_Request::getVar('friend_2','post');
		$friend_3 = Fuse_Request::getVar('friend_3','post');
		
		$friend_uid_1 = Fuse_Request::getVar('friend_uid_1','post');
		$friend_uid_2 = Fuse_Request::getVar('friend_uid_2','post');
		$friend_uid_3 = Fuse_Request::getVar('friend_uid_3','post');
		
		$randid = Fuse_Request::getVar("randid", "post");
		
		$model = new Fuse_Model();
		
		$nowtime = date("Y-m-d",time());	
		$checkShare = $model->getRow(" SELECT COUNT(*) as total FROM `weibo_share` 
		where `uid`='{$uid}' and `type`='{$category}' and left(`time`, 10)='{$nowtime}' ");
		
		//存温度限制玩乐次数
		if($category == 1){
			if($checkShare['total'] > 0){
				echo json_encode(array("result"=>"SHARED"));
	    		exit();	
			}
		} 
		
		//合成gif
		$image = $this->domakeGif($weibo_uid, $category);
		
		//存
		$share_content_warm = array(
			"fine"=>"今天天气好晴朗，处处好风光，今日气温[TEMP]，所以我的威能温暖存折上，又多出了[TEMP]的巨款，小伙伴们AT ，今天，你存了吗？", 
			"cloudy"=>"这天气，若是阳光明媚，举家出游，想必是极好的，但这阴天能有[TEMP]也不负恩泽。AT 说人话？我又去#威能温暖银行#存了[TEMP]度！", 
			"rainy"=>"下雨天了怎么办，我好想你，雨天你会想起谁？反正我会想起的是#威能温暖银行#，因为我的账户里又多了[TEMP]，小伙伴们AT ，今天，你存了吗？", 
			"snowy"=>"北国风光，千里冰封，万里雪飘……主席说的好啊，下雪天，就算千里冰封，万里雪飘，也一定要温度存进#威能温暖银行#，小伙伴们AT ，今天，你存了吗？"
		);
		
		//抱
		$share_content_embrace = array(
			"0"=>"修改修改修改，加班加班加班，工作每天都在“修”“加”，何时能真正休假？AT 求求求，求个拥抱好休假啊！快来#威能温暖银行#温暖我一下吧！", 
			"1"=>"此情可待成追忆，上课一定记笔记，接连挂科让我的心比喝了雪X更加透心凉，AT 还不快来#威能温暖银行#抱我一下，温暖我心窝？"
		);
		
		//团
//		$share_content_reunion = array(
//			"0"=>"床前明月光，疑是地上霜。举头望明月，低头月饼香，AT 送一个大大的[NAME]月饼给你们，要记得回家过中秋哦！", 
//			"1"=>"明月几时有，把酒问情缘，AT 你说咱中秋怎么过？来看看我在#威能温暖银行#定做的[NAME]月饼，可还合你的心意？"
//		);
		
		$share_content_reunion = array(
			"0"=>"小伙伴们，中秋节过了，嫦娥三号也要奔月了，AT 你还对着剩下的五仁摇头吗？不妨来尝尝我在#威能温暖银行#亲手DIY的[NAME]月饼，天下仅此一只，点慢了就吃不到咯！参与活动还有万元大奖可以拿哦！", 
			"1"=>"小伙伴们，中秋节过了，嫦娥三号也要奔月了，AT 你还对着剩下的五仁摇头吗？不妨来尝尝我在#威能温暖银行#亲手DIY的[NAME]月饼，天下仅此一只，点慢了就吃不到咯！参与活动还有万元大奖可以拿哦！"
		);
		
		$temp_current = $weathdata['temp_current'];
		
		if($category == 1){
			if(!isset($weathdata['weather_tpl'])){
				echo json_encode(array("result"=>"FAIL"));
    			exit();
			}else{
				$weather_tpl = $weathdata['weather_tpl'];
			}
		}
		
		$attime = "";
		$content = "微博分享文案";
		$url = "http://warmchina.vaillant.com.cn/";
		
		$at_list = " @".$friend_1."@".$friend_2."@".$friend_3;
		if($category == 1){
			$temp_current = 100;
			$url = "http://warmchina.vaillant.com.cn/warm.php?from=share";
			$content = str_replace("[TEMP]", $weathdata['temp_current']."℃", $share_content_warm[$weather_tpl]);
			$content = str_replace("AT", $at_list, $content);
		}elseif($category == 2){
			$temp_current = 100;
			$attime = time();
			$url = "http://warmchina.vaillant.com.cn/embrace.php?from=share&task=at&fuid=$uid&attime=$attime";
			$sharecontent = $share_content_embrace[$randid];
			$content = str_replace("AT", $at_list, $sharecontent);
		}elseif($category == 3){
			$temp_current = 100;
			$url = "http://warmchina.vaillant.com.cn/reunion.php?from=share";
			$sharecontent = $share_content_reunion[$randid];
			$content = str_replace("AT", $at_list, $sharecontent);
			$content = str_replace("[NAME]", $username, $content);
		}else{
			$temp_current = 30;
		}

		$content = $content." ".$url;
		if(!empty($image)){
			$msg = $weiboClient->upload($content,Config_App::homedir()."/combinimage/".$image);
		}else{
			$msg = $weiboClient->update($content);
		}

		if ($msg === false || $msg === null){
    		echo json_encode(array("result"=>"FAIL"));
    		exit();
    	}

    	if (isset($msg['error_code']) && isset($msg['error'])){
    		echo json_encode(array("result"=>"ERROR"));
    		exit();
    	}

    	$object = array();
    	if(!empty($friend_uid_1)){
    		$object["at"]     = $friend_uid_1.",".$friend_uid_2.",".$friend_uid_3;	
    	}
    	
    	if($attime){
    		$object["attime"]        = $attime;	
    	}
    	
    	$object["type"]        = $category;
    	$object["uid"]         = $uid;
    	$object["weibo_id"]    = $msg["idstr"];
    	$object["weibo_uid"]   = $weibo_uid;
    	$object["screen_name"] = $msg["user"]["screen_name"];
		$object["image"]       = $image;
    	$object["content"]     = $content;
   		$object['ip']          = $_SERVER['REMOTE_ADDR'];
    	$object['time']        = date("Y-m-d H:i:s");
   		$model->store("weibo_share",$object);
		
   		//增加温度
		$checkTemp = $model->getRow(" SELECT COUNT(*) as total FROM `user_temp` 
			where `uid`='{$uid}' and `category`='{$category}' and `type`='{$type}' and left(`time`, 10)='{$nowtime}' ");
		
		//2013-09-10 修改为不限制玩乐次数  除存温度
		$dataarr = array();
		$dataarr['uid']	     = $uid;
		$dataarr['category'] = $category;
		$dataarr['type']	 = $type;
		$dataarr['temp']	 = $temp_current;
		$dataarr['province'] = $weathdata['province'];
		$dataarr['city']	 = $weathdata['city'];
		$dataarr['ip']	     = $_SERVER['REMOTE_ADDR'];
		if($category == 1){
			if($checkTemp['total'] == 0 ){
				$model->store("user_temp", $dataarr);
				$model->getDb()->query("UPDATE `weibo_users` SET  `mytemp`=`mytemp`+$temp_current WHERE `uid` = '{$uid}'");
			}
		}else{
			$model->store("user_temp", $dataarr);
			$model->getDb()->query("UPDATE `weibo_users` SET  `mytemp`=`mytemp`+$temp_current WHERE `uid` = '{$uid}'");
		}
   		
    	echo json_encode(array("result"=>"OK"));
    	exit();

	}
	
	/**
	 * 下载图片の指定路径
	 * 当保存文件名称为空时则使用远程文件原来的名称
	 */
	function downloadFileCurl($url,$save_dir='',$filename='',$type=0){
		if(trim($url)==''){
			return false;
		}
		if(trim($save_dir)==''){
			$save_dir='./';
		}
		if(trim($filename)==''){//保存文件名
			$ext=strrchr($url,'.');
			if($ext!='.gif' && $ext!='.jpg'){
				return false;
			}
			$filename=time().$ext;
		}
		if(0!==strrpos($save_dir,'/')){
			$save_dir.='/';
		}
		//创建保存目录
		if(!file_exists($save_dir)&&!mkdir($save_dir,0777,true)){
			return false;
		}
		//获取远程文件所采用的方法
		if($type){
			$ch=curl_init();
			$timeout=5;
			curl_setopt($ch,CURLOPT_URL,$url);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
			$img=curl_exec($ch);
			curl_close($ch);
		}else{
			ob_start();
			readfile($url);
			$img=ob_get_contents();
			ob_end_clean();
		}
		$size=strlen($img);
		//文件大小
		$fp2=@fopen($save_dir.$filename,'wb');
		@fwrite($fp2,$img);
		@fclose($fp2);
		unset($img,$url);
		return array('file_name'=>$filename,'save_path'=>$save_dir.$filename);
	}
}

?>
