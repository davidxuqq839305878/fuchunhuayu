<?php
include_once(Config_App::homedir()."/site/plugin/sns/class/Sina.php");
 /**
 * @package     class
 * @author gary wang (wangbaogang123@hotmail.com)
 */
class rescueController extends Fuse_Controller
{
    private $config = null;
    /**
     * Constructor
     */
   public function __construct($config = array())
    {
        parent::__construct($config);
        $this->registerTask( 'index','index');
        $this->registerTask( 'weibo_callback','weibo_callback');
        $this->registerTask( 'seekhelp_success','seekhelp_success');
        $this->registerTask( 'seekhelp_success_share','seekhelp_success_share');
        $this->registerTask( 'request','request');
        $this->registerTask( 'callback','callback');
        $this->registerTask( 'check','check');
        $this->registerTask( 'logout','logout');
		$this->registerTask( 'cancel','cancel');
		$this->registerTask( 'share','share');
    }

    public function index(){
    	echo "seek_help";

        
    }


    public function weibo_callback(){
        $id = $_GET['id'];//GET获取冰冻列表中的id
        $model = new Fuse_Model();
        $list = $model->getRow("select * from `freeze_list` where `id`={$id}");
        $view = $this->createView();
        $view->id = $list['id'];
        $view->weibo_uidf = $list['weibo_uidf'];
        $view->weibo_uidt = $list['weibo_uidt'];
        $view->screen_name_f = $list['screen_name_f'];
        $view->screen_name_t = $list['screen_name_t'];
        $view->time = date('y-m-d H:i:s',$list['create_time']);
        $view->display("seekHelp/weibo_callback.html");

    }

    public function seekhelp_success(){
    	echo "seek_help_success";
    }

    public function seekhelp_success_share(){
    	echo "seek_help_success_share";
    }

}

?>
