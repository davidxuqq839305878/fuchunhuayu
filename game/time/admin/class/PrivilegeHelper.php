<?php
/**
 *
 * Privilege helper
 *
 * @package		classes
 * @subpackage	operator.navi
 * @author gary wang (wangbaogang123@hotmail.com)
 *
 */

class PrivilegeHelper extends Fuse_Model
{
	/**
	 * Check catalog privilege
	 */
	function checkPrivilege($action){
		$uid = Fuse_Cookie::getInstance()->user_id;
		if(empty($uid)){
			return 0;
		}
		$sql = "SELECT `group_id` FROM `admin_users` WHERE `user_id` = ?";
		$row = $this->getRow($sql,array($uid));

		$privilege = $this->getPrivilege($row["group_id"]);
		return array_search($action,$privilege);

	}

	/**
	 * Get user privileges
	 * return string
	 */
	function getPrivilege($group_id=0){
		$result = array();
		$sql = "SELECT * FROM `admin_group` WHERE `group_id` = ?";
		$row = $this->getRow($sql,array($group_id));
		if(!empty($row["privilege"])){
			$result = unserialize($row["privilege"]);
		}

		return $result;
	}


	function getNaviList(){
		$sql = "SELECT * FROM `admin_menu` WHERE `parent_menu_id`='0' AND `status`='1' ORDER BY `sort_id` ASC";
		$list = array();
		$navilist = $this->getRowSet($sql);
		for($i=0;$i<count($navilist);$i++){
			$sql = "SELECT * FROM `admin_menu` WHERE `parent_menu_id`='{$navilist[$i]['admin_menu_id']}' AND `status`='1' ORDER BY `sort_id` ASC";
			$navilist[$i]["list"] = $this->getRowSet($sql);
		}
		return $navilist;
	}

}

?>