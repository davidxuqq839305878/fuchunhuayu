<?php
/**
 * 
 * Controller for navigation
 *
 * @package		classes
 * @subpackage	admincp.index
 * @author gary wang
 * 
 */
include_once("PrivilegeHelper.php");

class UserController extends Fuse_Controller
{

	/**
	 * Constructor
	 *
	 * @params	array	Controller configuration array
	 */
	function __construct($config = array())
	{
		parent::__construct($config);
		$this->registerTask( 'login','login');
		$this->registerTask( 'logout','logout');
	}

	/**
	 * login
	 */
	function login()
	{
		$username = Fuse_Request::getVar("username","post");
		$passwd   = Fuse_Request::getVar("password","post");
		$formhash = Fuse_Request::getVar("formhash","post");
		$forward = Fuse_Request::getVar("forward");
		if(empty($forward))
		{
			$forward = Fuse_Request::getVar("HTTP_REFERER",'server');
		}

		$check_formhash = Config_App::formhash("admincp");
		
		if($check_formhash!=$formhash)
		{
			Fuse_Response::redirect($forward,"INVALID_FORM");
		}

		if(empty($username) || empty($passwd))
		{
			Fuse_Response::redirect($forward,"INVALID_INPUT");
		}
		$uid = Fuse_Cookie::getInstance()->user_id;
		if(!empty($uid))
		{
			Fuse_Cookie::getInstance()->user_id  = "";
			Fuse_Cookie::getInstance()->nickname = "";
		}

		$model = $this->createModel("Model_User",dirname( __FILE__ ));

		$row = $model->getLogin($username);

		if(empty($row)){
			Fuse_Response::redirect($forward,"LOGIN_ERROR");
		}

		$object = array();
		$object["last_login_ip"]   = Config_App::getIP();
		$object["last_login_time"] = date("Y-m-d H:i:s");

		$model->update($model->table["name"],$object,"`".$model->table["key"]."`='".$row["user_id"]."'");

		Fuse_Cookie::getInstance()->user_id  = $row["user_id"];
		Fuse_Cookie::getInstance()->nickname = $row["username"];

		Fuse_Response::redirect($forward);
	}

	/**
	 * logout
	 */
	function logout()
	{
		$forward = Fuse_Request::getVar("forward");
		if(empty($forward)){
			$forward = Fuse_Request::getVar("HTTP_REFERER",'server');
		}

		$uid = Fuse_Cookie::getInstance()->user_id;
		if(isset($uid)){
			Fuse_Cookie::getInstance()->user_id  = "";
			Fuse_Cookie::getInstance()->nickname = "";
			Fuse_Cookie::getInstance()->platform = "";
		}

		Fuse_Response::redirect($forward);

	}

}
