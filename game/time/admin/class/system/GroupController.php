<?php
 /**
 * Created 2012-11-25 11:24:41
 * 
 * @package		classes
 * @subpackage	operator.game
 * @author gary wang (wangbaogang123@hotmail.com)
 */
require_once("PrivilegeHelper.php");

class GroupController extends Fuse_Controller
{
	/**
	 * Constructor
	 */
	function __construct($config = array())
	{
		parent::__construct($config);
		
		$helper = new PrivilegeHelper();
		if(!($helper->checkPrivilege("system_group"))){
			Fuse_Response::redirect('/site/admincp/right.php','您没有权限访问该页面！');
		}
		
		
		$this->registerTask( 'search','jquerySearchList');
		$this->registerTask( 'list','itemList');
				
		$this->registerTask( 'create','create');
		$this->registerTask( 'insert','insert');
		$this->registerTask( 'modify','modify');
		$this->registerTask( 'update','update');
		$this->registerTask( 'delete','delete');
		$this->registerTask( 'export','export');

	}

	/**
	 * jquery 搜索
	 */


	function jquerySearchList()
	{
		$p = Fuse_Request::getVar('p','get');
		$word = urldecode(Fuse_Request::getVar('word','post'));

		if(empty($word)){
			$word = Fuse_Request::getVar('word','get');
		}

		$key = Fuse_Request::getVar('key','get');
		$model = $this->createModel('Model_Group',dirname( __FILE__ ));

		$where = '1';
		if($word){
			$where = "`{$key}` LIKE '%$word%'";
		}

		if(empty($p)){$p=1;}
		$baseurl = "list.php?key={$key}&word=".$word."&p=";
		$perpage = 20;
		
		$totalitems = $model->getTotal($where);
		$paginator = new Fuse_Paginator($totalitems,$p,$perpage,10);
		$limit = $paginator->getLimit();
		$itemList = $model->getList($limit["start"],$limit["offset"],$where);

		$view  = $this->createView();
		$view->title = "用户列表";	
		$view->forward = $baseurl;
		$view->total = $totalitems;
		$view->pageList = $paginator->getPages();
		$view->itemList = $itemList;
		$view->display("system/group/list.html");
	}
	
	/**
	 * 普通列表
	 */
	 function itemList()
	 {
		$p = Fuse_Request::getVar('p','get');

		$model = $this->createModel("Model_Group",dirname( __FILE__ ));
		
		$where = '1';
		if(empty($p)){$p=1;}
		$baseurl = "list.php?p=";
		$perpage = 20;
		
		$totalitems = $model->getTotal($where);
		$paginator = new Fuse_Paginator($totalitems,$p,$perpage,10);
		$limit = $paginator->getLimit();
		$itemList = $model->getList($limit["start"],$limit["offset"],$where);
		
		$view  = $this->createView();
		$view->pageList = $paginator->getPages();
		$view->itemList = $itemList;
		$view->total = $totalitems;
		$view->forward = $baseurl;
		$view->title = "用户组列表";
		$view->display("system/group/list.html");
	}

	function _getPrivilegeList($group_id=0){
		
		$helper = new PrivilegeHelper();
		if(empty($group_id)){
			$privilege = array();
		}else{
			$privilege = $helper->getPrivilege($group_id);
		}
		$list = $helper->getNaviList();
		for($i=0;$i<count($list);$i++){
			$check = false;
			for($q=0;$q<count($list[$i]["list"]);$q++){
				if (array_search($list[$i]["list"][$q]["action"], $privilege)) {
					$list[$i]["list"][$q]["checked"] = true;
					$check = true;
				}else{
					$list[$i]["list"][$q]["checked"] = false;
				}
			}
			if($check){
				$list[$i]["checked"] = true;
			}
		}
		
		return $list;
	}
	
	/**
	 * Create html
	 */
	function create(){

		$model = $this->createModel("Model_Group",dirname( __FILE__ ));
		$view  = $this->createView();
		$view->row = array("group_id"=>"","name"=>""); 
		$view->title = "用户组添加";
		$view->privilegeList = $this->_getPrivilegeList();
		$view->task = "insert";
		$view->display("system/group/create.html");
		
	}
	
	/**
	 * insert
	 */
	function insert(){
	
		$object = array();
		$object["name"]      = Fuse_Request::getVar("name","post");
		$privileges          = Fuse_Request::getVar("privileges","post");
		$object["privilege"] = serialize($privileges);
		$forward = "list.php";
		

		if(Fuse_Request::checkObject($object)){
			Fuse_Response::redirect($forward,"INVALID_INPUT");
		}
		
		$model = $this->createModel("Model_Group",dirname( __FILE__ ));

		if ($model->store($model->getTable(),$object)){
			Fuse_Response::redirect($forward, "success");
		}else{
			Fuse_Response::redirect($forward, "fail");
		}
	}
	
	/**
	 * modify
	 */
	function modify(){
		
		$model = $this->createModel("Model_Group",dirname( __FILE__ ));
		$id = Fuse_Request::getVar($model->getKey(),'get');
		$forward = Fuse_Request::getForward("forward");
		if(empty($id))
		{
			Fuse_Response::redirect($forward,"Need id!");
		}
		$row = $model->getRowOne($id);

		$view  = $this->createView();
		$view->row = $row;
		$view->title = "用户组修改";
		$view->privilegeList = $this->_getPrivilegeList($row["group_id"]);
		$view->task = "update";
		$view->display("system/group/create.html");
		
	}
	
	/**
	 * update
	 */
	function update(){
		
		$model = $this->createModel("Model_Group",dirname( __FILE__ ));
		
		$id = Fuse_Request::getVar($model->getKey(),'request');
		$forward = "list.php";
		if(empty($id))
		{
			Fuse_Response::redirect($forward,"Need id!");
		}
		$object = array();
		$object["name"]      = Fuse_Request::getVar("name","post");
		$privileges          = Fuse_Request::getVar("privileges","post");
		$object["privilege"] = serialize($privileges);			
		
		if(Fuse_Request::checkObject($object)){
			Fuse_Response::redirect($forward,"INVALID_INPUT");
		}
		
		if ($model->update($model->getTable(),$object,"`".$model->getKey()."`='{$id}'")){
			Fuse_Response::redirect($forward, "success");
		}else{
			Fuse_Response::redirect($forward, "success");
		}
	}
	
	/**
	 * delete one
	 */
	function delete(){
	
		$model = $this->createModel("Model_Group",dirname( __FILE__ ));

		$id = Fuse_Request::getVar($model->getKey(),'request');
		$forward = "list.php";
		if(empty($id))
		{
			Fuse_Response::redirect($forward,"Need id!");
		}
		
		
		$row = $model->getRowOne($id);
		
		if ($model->delete($id)){
			Fuse_Response::redirect($forward, "success");
		}else{
			Fuse_Response::redirect($forward, "fail");
		}
	}

	/**
	 * export excel
	 */
	function export(){

		

	}

}

?>