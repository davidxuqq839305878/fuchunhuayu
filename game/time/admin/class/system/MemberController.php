<?php
include_once("PrivilegeHelper.php");

 /**
 * Created 2012-11-25 11:24:41
 *
 * @package		classes
 * @subpackage	operator.game
 * @author gary wang (wangbaogang123@hotmail.com)
 */
class MemberController extends Fuse_Controller
{

	/**
	 * Constructor
	 */
	function __construct($config = array())
	{
		parent::__construct($config);

		$helper = new PrivilegeHelper();
		
		if(!($helper->checkPrivilege("system_member"))){
			Fuse_Response::redirect('/site/admincp/right.php','您没有权限访问该页面！');
		}
		

		$this->registerTask( 'search','jquerySearchList');
		$this->registerTask( 'list','itemList');

		$this->registerTask( 'create','create');
		$this->registerTask( 'insert','insert');
		$this->registerTask( 'modify','modify');
		$this->registerTask( 'update','update');
		$this->registerTask( 'delete','delete');

		$this->registerTask( 'export','export');
	}

	/**
	 * jquery 搜索
	 */


	function jquerySearchList()
	{
		$p = Fuse_Request::getVar('p','get');
		$word = urldecode(Fuse_Request::getVar('word','post'));

		if(empty($word)){
			$word = Fuse_Request::getVar('word','get');
		}

		$key = Fuse_Request::getVar('key','get');
		$model = $this->createModel('Model_Member',dirname( __FILE__ ));

		$where = '1';
		if($word){
			$where = "`{$key}` LIKE '%$word%'";
		}

		if(empty($p)){$p=1;}
		$baseurl = "list.php?key={$key}&word=".$word."&p=";
		$perpage = 20;

		$totalitems = $model->getTotal($where);
		$paginator = new Fuse_Paginator($totalitems,$p,$perpage,10);
		$limit = $paginator->getLimit();
		$itemList = $model->getList($limit["start"],$limit["offset"],$where);

		$view  = $this->createView();
		$view->title = "管理员列表";
		$view->forward = $baseurl;
		$view->total = $totalitems;
		$view->start = ($p-1)*$perpage;
		$view->pageList = $paginator->getPages();
		$view->itemList = $itemList;
		$view->display("system/member/list.html");
	}

	/**
	 * 普通列表
	 */
	 function itemList()
	 {
		$p = Fuse_Request::getVar('p','get');
		$username = Fuse_Request::getVar('username','request');

		$model = $this->createModel("Model_Member",dirname( __FILE__ ));

		$where = '1';
		if(empty($p)){$p=1;}
		if(!empty($username)){
			$baseurl = "list.php?username=$username&p=";
		}else{
			$baseurl = "list.php?p=";
		}
		$perpage = 20;

		if(!empty($username)){
			$where .=" AND `username` LIKE '%".$username."%'";
		}

		$totalitems = $model->getTotal($where);
		$paginator = new Fuse_Paginator($totalitems,$p,$perpage,10);
		$limit = $paginator->getLimit();
		$itemList = $model->getList($limit["start"],$limit["offset"],$where);



		$view  = $this->createView();
		$view->pageList = $paginator->getPages();
		$view->itemList = $itemList;
		$view->total = $totalitems;
		$view->start = ($p-1)*$perpage;
		$view->forward = $baseurl;
		$view->baseurl = $baseurl;
		$view->username = $username;
		$view->title = "管理员列表";
		$view->display("system/member/list.html");
	}

	/**
	 * Create html
	 */
	function create(){

		$model = $this->createModel("Model_Member",dirname( __FILE__ ));
		$view  = $this->createView();
		$view->row = array("username"=>"","group_id"=>"","status"=>"","user_id"=>"");
		$view->title = "管理员添加";
		$view->task = "insert";
		$view->groupList    = $model->getRowset("SELECT * FROM `admin_group` WHERE 1");
		$view->display("system/member/create.html");

	}

	/**
	 * insert
	 */
	function insert(){

		$object = array();
		$object["username"]   = Fuse_Request::getVar("username","post");
		$object["password"]   = Fuse_Request::getVar("password","post");
		$object["group_id"]   = Fuse_Request::getVar("group_id","post");

		$forward = "list.php";

		if(Fuse_Request::checkObject($object)){
			Fuse_Response::redirect($forward,"INVALID_INPUT");
		}

		$object["status"]    = Fuse_Request::getVar("status","post");

		$model = $this->createModel("Model_Member",dirname( __FILE__ ));

		$checkrow = $model->getRow("SELECT COUNT(*) AS total FROM `admin_users` WHERE `username`=?",array($object["username"]));
		
		if($checkrow["total"]>0){
			Fuse_Response::redirect($forward,"EXISTS_USERNAME");
		}

		$object["ip"]   = $_SERVER["REMOTE_ADDR"];
		$object["time"] = date('Y-m-d H:i:s');

		$object["password"] = md5($object["password"]);

		$object["group_id"] = "1";

		if ($model->store($model->getTable(),$object)){
			Fuse_Response::redirect($forward, "success");
		}else{
			Fuse_Response::redirect($forward, "fail");
		}
	}

	/**
	 * modify
	 */
	function modify(){

		$model = $this->createModel("Model_Member",dirname( __FILE__ ));
		$id = Fuse_Request::getVar($model->getKey(),'get');
		$forward = Fuse_Request::getForward("forward");
		if(empty($id))
		{
			Fuse_Response::redirect($forward,"Need id!");
		}
		$row = $model->getRowOne($id);

		$view  = $this->createView();
		$view->row = $row;
		$view->groupList = $model->getRowset("SELECT * FROM `admin_group` WHERE 1");
		$view->title = "管理员修改";
		$view->task = "update";
		$view->display("system/member/create.html");

	}

	/**
	 * update
	 */
	function update(){

		$model = $this->createModel("Model_Member",dirname( __FILE__ ));
		$id = Fuse_Request::getVar($model->getKey(),'request');
		$forward = "list.php";
		if(empty($id))
		{
			Fuse_Response::redirect($forward,"Need id!");
		}
		$object = array();
		//$object["username"]   = Fuse_Request::getVar("username","post");
		$password   = Fuse_Request::getVar("password","post");
		$object["group_id"]   = Fuse_Request::getVar("group_id","post");

		if(Fuse_Request::checkObject($object)){
			Fuse_Response::redirect($forward,"INVALID_INPUT");
		}

		$object["status"]    = Fuse_Request::getVar("status","post");

		if(!empty($password)){
			$object["password"] = md5($password);
		}

		$object["group_id"] = "1";

		//$row = $model->getRowOne($id);
		
		if ($model->update($model->getTable(),$object,"`".$model->getKey()."`='{$id}'")){
			Fuse_Response::redirect($forward, "success");
		}else{
			Fuse_Response::redirect($forward, "success");
		}
	}

	/**
	 * delete one
	 */
	function delete(){

		$model = $this->createModel("Model_Member",dirname( __FILE__ ));
		$id = Fuse_Request::getVar($model->getKey(),'request');
		$forward = "list.php";
		if(empty($id))
		{
			Fuse_Response::redirect($forward,"Need id!");
		}

		$row = $model->getRowOne($id);

		if($row["group_id"]==1){
			Fuse_Response::redirect($forward, "delete not allow");
		}else{
			if(Fuse_Cookie::getInstance()->user_id!=$row["user_id"]){
				Fuse_Response::redirect($forward, "delete not allow");
			}
		}
		
		if ($model->delete($id)){
			Fuse_Response::redirect($forward, "success");
		}else{
			Fuse_Response::redirect($forward, "fail");
		}
	}

	/**
	 * export excel
	 */
	function export(){


	}

}

?>