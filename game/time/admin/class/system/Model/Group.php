<?php
/**
 * Example model
 * Created 2012-11-25 11:24:42
 * @package		classes
 * @subpackage	operator.game
 * @author gary wang (wangbaogang123@hotmail.com)
 * 
 */
class Model_Group extends Fuse_Model
{

	/**
	 * array
	 */
	public $table = array("name"=>"admin_group","key"=>"group_id");

	
	public function __construct($config=array())
	{
		parent::__construct($config);
	}

	public function getList($start=0,$per_page=10,$where=1)
	{
		$list = array();
		$sql = " SELECT * FROM `{$this->table['name']}` WHERE {$where} ORDER BY `{$this->table['key']}` DESC";

		if($start>=0 && !empty($per_page)){
			$sql .= " LIMIT {$start},{$per_page}";
		}
		
		if( ($stmt = $this->db->query($sql)) )
		{
			while ( $row = $stmt->fetch() )
			{
				$list[] = $row;
			}
		}
		return $list;
	}
	
	public function getTotal($where=1)
	{
		$total = 0;
        
		$sql = "SELECT COUNT(*) AS total FROM `{$this->table['name']}` WHERE {$where}";
		if(($stmt = $this->db->query($sql)))
		{
			if($row = $stmt->fetch())
			{
				$total = $row['total'];
			}
		}
		
		return $total;
	}
	
	public function getRowOne($id)
	{
		$list = null;
		
		$sql = "SELECT * FROM `{$this->table['name']}` WHERE `{$this->table['key']}`='{$id}'";
		
		if(($stmt = $this->db->query($sql)))
		{
			if($row = $stmt->fetch())
			{
				$list = $row;
			}
		}
		return $list;
	}
 
	public function delete($id)
	{
		$sql = "DELETE FROM `{$this->table['name']}` WHERE `{$this->table['key']}`='{$id}'";
		return $this->db->query($sql);
	}
	
	public function getKey()
	{
		return $this->table['key'];
	}
	
	public function getTable()
	{
		return $this->table['name'];
	}
}
?>