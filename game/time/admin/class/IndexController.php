<?php
/**
 * 
 * Controller for navigation
 *
 * @package		classes
 * @subpackage	admincp.index
 * @author gary wang (wangbaogang123@hotmail.com)
 * 
 */
include_once("PrivilegeHelper.php");




class IndexController extends Fuse_Controller
{

    function getCurrentDateId(){

        //    2014年3月10日-2014年4月6日
        //    第1期花语收集：3月10日-3月23日
        //    第2期花语收集：3月24日-4月6日
        $nowDateStr = date('Y-m-d H:i:s',time());
        $nowTrueDate = strtotime($nowDateStr); //当前时间戳

        $timestr_0 = '2014-03-09 23:59:59';
        $time_0 = strtotime($timestr_0);

        $timestr_1_start = '2014-03-10 00:00:00';
        $timestr_1_end = '2014-03-23 23:59:59';

        $time_1_start = strtotime($timestr_1_start);
        $time_1_end = strtotime($timestr_1_end);


        $timestr_2_start = '2014-03-24 00:00:00';
        $timestr_2_end = '2014-04-06 23:59:59';

        $time_2_start = strtotime($timestr_2_start);
        $time_2_end = strtotime($timestr_2_end);

        $timestr_3 = '2014-04-07 00:00:00';


        if($nowTrueDate < $time_0){
            $dateid = 0;
        }else if($nowTrueDate < $time_1_end){
            $dateid = 1;
        }else if($nowTrueDate < $time_2_end){
            $dateid = 2;
        }else{
            $dateid = 3;
        }
        return $dateid;
    }

    /**
	 * Constructor
	 *
	 * @params	array	Controller configuration array
	 */
	function __construct($config = array())
	{
		parent::__construct($config);
        
		$this->registerTask( 'index','display');
		$this->registerTask( 'add','add');
		$this->registerTask( 'edit','edit');
		$this->registerTask( 'del','del');
		$this->registerTask( 'update','update');
		$this->registerTask( 'time_limit','time_limit');
        $this->registerTask( 'login','login');
        $this->registerTask( 'userlist','userlist');
        $this->registerTask( 'share_status','share_status');
    }

	/**
	 * index
	 */
	function display()
	{

		$model = new Fuse_Model();
		$row1 = $model->getRowSet("SELECT * FROM `time_limit` where `level`=?",array(4));
		$row2 = $model->getRowSet("SELECT * FROM `time_limit` where `level`=?",array(5));
		$row3 = $model->getRowSet("SELECT * FROM `time_limit` where `level`=?",array(6));
        $row4 = $model->getRowSet("SELECT * FROM `time_limit` where `level`=?",array(7));
        $row5 = $model->getRowSet("SELECT * FROM `time_limit` where `level`=?",array(8));

        $nowTrueDate = strtotime(date('Y-m-d H:i:s', time()));

		$view = $this->createView();
        $view->game_host_url = Config_App::$game_host_url;
        $view->row1 = $row1;
		$view->row1_len = count($row1);
		$view->row2 = $row2;
		$view->row2_len = count($row2);
		$view->row3 = $row3;
		$view->row3_len = count($row3);
        $view->row4 = $row4;
        $view->row4_len = count($row4);
        $view->row5 = $row5;
        $view->row5_len = count($row5);
        $view->dateid = getCurrentDateId();
        $view->nowTrueDate = $nowTrueDate;
		$view->display('list.html');
	}
    function share_status(){

//        $Pagemodel = new Fuse_Model();
//        $pageRow = $Pagemodel->getRowSet("SELECT * FROM `share_status` AS table1 LEFT JOIN user_flower_activity AS table2 ON table1.wxid = table2.wxid WHERE table1.dateid = '$dateid' ORDER BY table1.id DESC");
//
//        /**分页*/
//        $pageCount = 0;
//        $pageLimit = 20; //每页20条记录
//        $page = $_GET['page']?$_GET['page']:1;
//        $totalCount = count($pageRow);//总计多少条记录
//        if($totalCount < $pageCount){
//            $pageCount = 1;
//        }else{
//            $pageCount = ceil($totalCount/$pageCount);//总计多少页
//        }


        $dateid = $_GET['dateid'] ? $_GET['dateid'] : 1;
        $export = $_GET['export'] ? $_GET['export'] : 0;
        $all = $_GET['all'] ? $_GET['all'] : 0;
        $filter = $_GET['filter'] ? $_GET['filter'] : 0;

        if($all == 1){
            //显示所有的数据
            $startTime = '2014-03-10 00:00:00';
            $endTime = date('Y-m-d H:i:s',time());
        }else{
           //显示限定时间的数据
           $startTime = $_GET['start'] ? ($_GET['start'].' '.$_GET['start_time_h'].':'.$_GET['start_time_m'].':00') : date('Y-m-d H:i:s',time() - 1 * 24 * 60 * 60);
           $endTime = $_GET['end'] ? ($_GET['end'].' '.$_GET['end_time_h'].':'.$_GET['end_time_m'].':00') : date('Y-m-d H:i:s',time());
        }

        if($filter == 1){
            $defaultAddSql = 'AND table2.name !=\'\' AND table2.tel !=\'\'';
        }else{
            $defaultAddSql = '';
        }



        $model = new Fuse_Model();
        $date = date('Y-m-d H:i:s',time());
        $shareCount = 0;//初始化，记录分享至朋友圈的行为次数
        $joinCount = 0;//初始化，记录朋友圈有人来帮忙行为次数

        $row = $model->getRowSet("SELECT * FROM `share_status` AS table1 LEFT JOIN user_flower_activity AS table2 ON table1.wxid = table2.wxid WHERE table1.dateid = '$dateid' AND UNIX_TIMESTAMP(time) >= UNIX_TIMESTAMP('$startTime') AND UNIX_TIMESTAMP(time)  <= UNIX_TIMESTAMP('$endTime') $defaultAddSql ORDER BY table1.id DESC");


        /**查询总记录数**/
        $modelWxid = new Fuse_Model();
        $rowWxidCount = $modelWxid->getRowSet("SELECT COUNT(DISTINCT table1.wxid) AS WxidCount FROM `share_status` AS table1 LEFT JOIN user_flower_activity AS table2 ON table1.wxid = table2.wxid WHERE table1.dateid = '$dateid' AND UNIX_TIMESTAMP(time) >= UNIX_TIMESTAMP('$startTime') AND UNIX_TIMESTAMP(time)  <= UNIX_TIMESTAMP('$endTime') $defaultAddSql ORDER BY table1.id DESC");
        /**查询总记录数**/



        foreach($row as &$val){
            $type = $val['type'];
            if($type == 'share'){
                $val['type'] = '分享至朋友圈';
                $shareCount ++;
            }else  if($type == 'join'){
                $val['type'] = '朋友圈有人来帮忙';
                $joinCount ++;
            }else{

            }
            //print_r($val);
        }
        unset($val);
        //print_r($rowWxidCount[0]['WxidCount']);

        $view = $this->createView();
        $view->row = $row;
        $view->row_len = count($row);
        $view->dateid = getCurrentDateId();
        $view->game_host_url = Config_App::$game_host_url;
        $view->getDateid = $dateid;
        $view->count = count($row);
        $view->now_time = $date;
        $view->all = $all;
        $view->filter = $filter;

        $view->startTime = $startTime;
        $view->time_start_date = substr($startTime,0,10);
        $view->time_start_hour = substr($startTime,11,2);
        $view->time_start_minutes = substr($startTime,14,2);
        $view->endTime = $endTime;
        $view->time_end_date = substr($endTime,0,10);
        $view->time_end_hour = substr($endTime,11,2);
        $view->time_end_minutes = substr($endTime,14,2);

        $view->shareCount = $shareCount;
        $view->joinCount = $joinCount;
        $view->wxidCount = $rowWxidCount[0]['WxidCount'];
        if($export == 1){
            header("Content-type:application/vnd.ms-excel;charset=utf-8");
            header("Content-Disposition:filename=export-$dateid-$flowercount-$userlist-$date.xls");
            $view->display('share_status_export.html');

        }else{
            $view->display('share_status.html');
        }

    }
    function userlist()
    {

        $all = isset($_GET['all']) ? $_GET['all'] : 1;
        /*参与活动的时间限制条件*/
        $startTime = $_GET['start'] ? ($_GET['start'].' '.$_GET['start_time_h'].':'.$_GET['start_time_m'].':00') : date('Y-m-d H:i:s',time() - 1 * 24 * 60 * 60);
        $endTime = $_GET['end'] ? ($_GET['end'].' '.$_GET['end_time_h'].':'.$_GET['end_time_m'].':00') : date('Y-m-d H:i:s',time());
        if($all == 1){
            $andTimeSql ="";
        }else{
            $andTimeSql = "AND UNIX_TIMESTAMP(table1.jointime) >= UNIX_TIMESTAMP('$startTime') AND UNIX_TIMESTAMP(table1.jointime)  <= UNIX_TIMESTAMP('$endTime')";
        }

        print_r('all:'.$all);

        $dateid = $_GET['dateid'] ? $_GET['dateid'] : 1;
        $flowercount = $_GET['flowercount'] ? $_GET['flowercount'] : 'all';
        $nameNotNull = $_GET['nameNotNull'] ? $_GET['nameNotNull'] : 0;
        $export = $_GET['export'] ? $_GET['export'] : 0;
        $date = date('Y-m-d',time());

        if($nameNotNull == 0){
            $defaultAddSql = 'AND table1.name !=\'\' AND table1.tel !=\'\'';
        }else{
            $defaultAddSql = '';
        }
        $model = new Fuse_Model();
        $row = $model->getRowSet("SELECT * FROM `user_flower_activity` AS table1 LEFT JOIN activity_info AS table2 ON table1.wxid = table2.wxid WHERE table2.dateid = '$dateid' $defaultAddSql $andTimeSql  ORDER BY LENGTH(table2.activityarr) DESC");

        //print_r("SELECT * FROM `user_flower_activity` AS table1 LEFT JOIN activity_info AS table2 ON table1.wxid = table2.wxid WHERE table2.dateid = '$dateid' $defaultAddSql $andTimeSql  ORDER BY LENGTH(table2.activityarr) DESC");

        /*
         * Array
            (
                [id] => 9
                [wxid] => oBf_qJVNMujFBLryxzwOijkyDCBM000
                [name] =>
                [jointime] => 2014-03-01 16:01:51
                [lasttime] => 2014-03-01 16:01:51
                [tel] =>
                [awardcount] => 0
                [dateid] => 0
                [activityarr] => a:3:{i:0;a:1:{s:4:"time";s:19:"2014-02-28 19:22:43";}i:1;a:1:{s:4:"time";s:19:"2014-02-28 19:22:43";}i:2;a:1:{s:4:"time";s:19:"2014-02-28 19:22:43";}}
            )
         * */
        foreach($row as &$val){

            $arr = unserialize($val['activityarr']);
            $count = count($arr) ;//对于数组，返回其元素的个数，对于其他值，返回 1
            if($count == 1){
                $val['count'] = 0;
            }else{
                $val['count'] = $count;
            }
            //print_r($val);
        }
        unset($val);

        //过滤
        if($flowercount != 'all'){
            foreach($row as $key=>$val){
                if($val['count'] != $flowercount ){
                    unset($row[$key]);
                }
            }
        }

        $view = $this->createView();
        $view->game_host_url = Config_App::$game_host_url;
        $view->count = count($row);//计算多少条记录
        $view->getDateid = $dateid;
        $view->flowercount = $flowercount;
        $view->row = $row;
        $view->all = $all;
        $view->startTime = $startTime;
        $view->time_start_date = substr($startTime,0,10);
        $view->time_start_hour = substr($startTime,11,2);
        $view->time_start_minutes = substr($startTime,14,2);
        $view->endTime = $endTime;
        $view->time_end_date = substr($endTime,0,10);
        $view->time_end_hour = substr($endTime,11,2);
        $view->time_end_minutes = substr($endTime,14,2);
        $view->now_time = date('Y-m-d H:i:s',time());
        $view->row_len = count($row);
        $view->dateid = getCurrentDateId();


        if($export == 1){
            header("Content-type:application/vnd.ms-excel;charset=utf-8");
            header("Content-Disposition:filename=export-$dateid-$flowercount-$userlist-$date.xls");
            $view->display('export.html');

        }else{
           $view->display('userlist.html');
        }

    }

    function login(){

        $view = $this->createView();
        $view->game_host_url = Config_App::$game_host_url;
        $view->display('login.html');
        $object = array();
        $object['username'] = $_POST['username'];
        $object['password'] = $_POST['password'];
        if( $object['username']=='admin' && $object['password'] == '123456'){
            echo "<script>";
            echo "location='".Config_App::$game_host_url."/time/admin/index.php'";
            echo "</script>";
        }else{
            if(isset($_POST['username'])){
                echo "<script>";
                echo "alert('帐号或者密码错误，请重新输入')";
                echo "</script>";
            }else{

            }
        }
    }

	function add(){

        $view = $this->createView();
		$model = new Fuse_Model();
		$row1 = $model->getRowSet("SELECT * FROM `time_limit` where `level`=?",array(4));
		$row2 = $model->getRowSet("SELECT * FROM `time_limit` where `level`=?",array(5));
		$row3 = $model->getRowSet("SELECT * FROM `time_limit` where `level`=?",array(6));
        $row4 = $model->getRowSet("SELECT * FROM `time_limit` where `level`=?",array(7));
        $row5 = $model->getRowSet("SELECT * FROM `time_limit` where `level`=?",array(8));
        $view->game_host_url = Config_App::$game_host_url;
        $view->row1 = $row1;
		$view->row1_len = count($row1);
		$view->row2 = $row2;
		$view->row2_len = count($row2);
		$view->row3 = $row3;
		$view->row3_len = count($row3);
        $view->row4 = $row4;
        $view->row4_len = count($row4);
        $view->row5 = $row5;
        $view->row5_len = count($row5);
        $view->dateid = getCurrentDateId();

        $view->display('index.html');


	}

	function edit(){
				$id = $_GET['id'];
				$model = new Fuse_Model();
				$row = $model->getRow("SELECT * FROM `time_limit` where `id`=?",array($id));
				$view = $this->createView();
				$view->row = $row;
                $view->game_host_url = Config_App::$game_host_url;
                $view->time_start = $row['time_start'];
                $view->time_end = $row['time_end'];
                $view->time_start_date = substr($row['time_start'], 0, 10);
                $view->time_start_hour = substr($row['time_start'], 11, 2);
                $view->time_start_minutes = substr($row['time_start'], 14, 2);
                $view->time_end_date = substr($row['time_end'], 0, 10);
                $view->time_end_hour = substr($row['time_end'], 11, 2);
                $view->time_end_minutes = substr($row['time_end'], 14, 2);
                $view->level = $row['level'];
                if($row['level']==4){
                    $view->legend = '最多获得四朵';
                }else if($row['level']==5){
                    $view->legend = '最多获得五朵';
                }else if($row['level']==6){
                    $view->legend = '最多获得六朵';
                }else if($row['level']==7){
                    $view->legend = '最多获得七朵';
                }else{
                    $view->legend = '最多获得八朵';
                }
                  $view->dateid = getCurrentDateId();

        $view->display('edit.html');
	}

	function del(){
				$id = $_GET['id'];
				$model = $this->createModel("Model_User",dirname( __FILE__ ));
				$row = $model->del("time_limit",$id);
				header("location:".Config_App::$game_host_url."/time/admin/index.php");
	}

	function update(){

				$id = $_POST['id'];
				$model = new Fuse_Model();
				$object = array();
				$object['time_start'] = $_POST['time_start']." ".$_POST['time_start_h'].":".$_POST['time_start_m'];
				$object['time_end'] = $_POST['time_end']." ".$_POST['time_end_h'].":".$_POST['time_end_m'];
				$object['num'] = $_POST['num'];
				$a = $model->update("time_limit", $object, " `id`='{$id}' ");

					echo "<script>";
					echo "location='".Config_App::$game_host_url."/time/admin/index.php'";
					echo "</script>";
				

	}


	function time_limit(){

		$length = count($_POST['g_start']);
		$model = new Fuse_Model();
		$arr = array();
		$time_start = array();
		$time_end = array();
		for($i=0;$i<$length;$i++){

			if(!empty($_POST['g_start'][0])){
				$time_start[4][$i] = $_POST['g_start'][$i]." ".$_POST['g_start_time_h'][$i].":".$_POST['g_start_time_m'][$i].":00";
				$time_end[4][$i] = $_POST['g_end'][$i]." ".$_POST['g_end_time_h'][$i].":".$_POST['g_end_time_m'][$i].":00";
				$arr[4][$i] = array('time_start'=>$time_start[4][$i],'time_end'=>$time_end[4][$i],'level'=>4,'num'=>$_POST['g_num'][$i]);
			}

			if(!empty($_POST['x_start'][0])){

			$time_start[5][$i] = $_POST['x_start'][$i]." ".$_POST['x_start_time_h'][$i].":".$_POST['x_start_time_m'][$i].":00";
			$time_end[5][$i] = $_POST['x_end'][$i]." ".$_POST['x_end_time_h'][$i].":".$_POST['x_end_time_m'][$i].":00";
			$arr[5][$i] = array('time_start'=>$time_start[5][$i],'time_end'=>$time_end[5][$i],'level'=>5,'num'=>$_POST['x_num'][$i]);
			}

			if(!empty($_POST['s_start'][0])){
			$time_start[6][$i] = $_POST['s_start'][$i]." ".$_POST['s_start_time_h'][$i].":".$_POST['s_start_time_m'][$i].":00";
			$time_end[6][$i] = $_POST['s_end'][$i]." ".$_POST['s_end_time_h'][$i].":".$_POST['s_end_time_m'][$i].":00";
			$arr[6][$i] = array('time_start'=>$time_start[6][$i],'time_end'=>$time_end[6][$i],'level'=>6,'num'=>$_POST['s_num'][$i]);
			}

            if(!empty($_POST['t_start'][0])){
                $time_start[7][$i] = $_POST['t_start'][$i]." ".$_POST['t_start_time_h'][$i].":".$_POST['t_start_time_m'][$i].":00";
                $time_end[7][$i] = $_POST['t_end'][$i]." ".$_POST['t_end_time_h'][$i].":".$_POST['t_end_time_m'][$i].":00";
                $arr[7][$i] = array('time_start'=>$time_start[7][$i],'time_end'=>$time_end[7][$i],'level'=>7,'num'=>$_POST['t_num'][$i]);
            }

            if(!empty($_POST['u_start'][0])){
                $time_start[8][$i] = $_POST['u_start'][$i]." ".$_POST['u_start_time_h'][$i].":".$_POST['u_start_time_m'][$i].":00";
                $time_end[8][$i] = $_POST['u_end'][$i]." ".$_POST['u_end_time_h'][$i].":".$_POST['u_end_time_m'][$i].":00";
                $arr[8][$i] = array('time_start'=>$time_start[8][$i],'time_end'=>$time_end[8][$i],'level'=>8,'num'=>$_POST['u_num'][$i]);
            }


        }

		foreach($arr as $val){
			foreach($val as $v){
				$model->store("time_limit",$v);
			}			
		}	

		echo "<script>";
		echo "location='".Config_App::$game_host_url."/time/admin/index.php'";
		echo "</script>";
	}


	function _getPrivilegeList($group_id=0){
		
		$helper = new PrivilegeHelper();
		if(empty($group_id)){
			$privilege = array();
		}else{
			$privilege = $helper->getPrivilege($group_id);
		}
		$list = $helper->getNaviList();
		
		$rowset = array();

		for($i=0;$i<count($list);$i++){
			$check = false;
			$row = array();
			for($q=0;$q<count($list[$i]["list"]);$q++){
				if (array_search($list[$i]["list"][$q]["action"], $privilege)) {
					$row[] = $list[$i]["list"][$q];
					$check = true;
				}
			}
			if($check){
				$list[$i]["list"] = $row;
				$rowset[] = $list[$i];
			}
		}
		
		return $rowset;
	}

	/**
	 * index
	 */
	function _index()
	{
        $uid      = Fuse_Cookie::getInstance()->user_id;
        $nickname = Fuse_Cookie::getInstance()->nickname;


		$model = new Fuse_Model();
		$row = $model->getRow("SELECT * FROM `admin_users` WHERE `user_id`=?",array($uid));
    	$phelper = new PrivilegeHelper();

		//var_dump($this->_getPrivilegeList($row["group_id"]));

		
		$privilege = $phelper->getPrivilege($row["group_id"]);
		/*
		if(count($privilege)==0||empty($uid)){
			Fuse_Response::redirect("/","您没有权限访问！");
		}
		*/
		
		$view = $this->createView();
		$view->user_id      = $uid;
		$view->nickname = $nickname;
		//$view->naviList = $phelper->getNaviList();
		$view->naviList =  $this->_getPrivilegeList($row["group_id"]);
		$view->display('index.html');
	}

}
