<?php

 /**
 * Created 2012-11-25 11:24:41
 * 
 * @package		classes
 * @subpackage	operator.game
 * @author gary wang (wangbaogang123@hotmail.com)
 */
include_once("PrivilegeHelper.php");

class WeiboController extends Fuse_Controller
{
	private $helper;

	/**
	 * Constructor
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
		
		$this->helper = new PrivilegeHelper();

		if(!($this->helper->checkPrivilege("app_weibo"))){
			Fuse_Response::redirect('/site/admincp/right.php','您没有权限访问该页面！');
		}
		
		$this->registerTask( 'list','weibo_list');
		$this->registerTask( 'dataexport','dataexport');
	}

	public function weibo_list()
	{

		$p = Fuse_Request::getVar('p','get');

		$key       = Fuse_Request::getVar('key','get');
		$word      = urldecode(Fuse_Request::getVar('word','get'));
		
		$start_date = Fuse_Request::getVar('start_date','get');
		$end_date   = Fuse_Request::getVar('end_date','get');
		
		$model = $this->createModel("Model_Weibo",dirname( __FILE__ ));

		$uri = new Fuse_Uri("weibolist.php");

		$search = array();
		$where_list = array();

		if(!empty($key) && !empty($word)){
			$uri->setVar($key,$word);
			$search["key"] = $key;
			$search["word"] = $word;
			$where_list[] = "`{$key}` LIKE '%{$word}%'";
		}

		if(!empty($start_date)){
			$uri->setVar("start_date",$start_date);
			$where_list[] = "`time` >= '{$start_date}'";
		}

		if(!empty($end_date)){
			$uri->setVar("end_date",$end_date);
			$where_list[] = "`time` <= '{$end_date}'";
		}

		$where = '1';
		if(empty($p)){$p=1;}

		$uri->setVar("p",'');
		$baseurl = $uri->toString();
		
		if(!empty($where_list)){
			$where = implode(' AND ',$where_list);
		}
		
		$perpage = 20;

	
		$totalitems = $model->getTotal($where);
		$paginator = new Fuse_Paginator($totalitems,$p,$perpage,10);
		$limit = $paginator->getLimit();
		$itemList = $model->getList($limit["start"],$limit["offset"],$where);
		

		$view  = $this->createView();
		$view->pageList = $paginator->getPages();
		$view->search   = $search;
		$view->p     = $p;
		$view->itemList = $itemList;
		$view->total    = $totalitems;
		$view->baseurl  = $baseurl;
		$view->title = "微博用户列表";
		$view->display("app/weibouserlist.html");

	}
	
	function dataexport(){

		require_once("Excel/Writer.php");
		$model = $this->createModel("Model_Weibo",dirname( __FILE__ ));

		$itemlist = $model->getList(0, 0);

		$workbook = new Spreadsheet_Excel_Writer();

        //sending HTTP headers
        $workbook->send(date("Ymd")."_weibouser.xls");
		$worksheet = $workbook->addWorksheet("weibouser");

		$worksheet->write(0, 0, iconv("UTF-8","GBK//IGNORE", "微博UID"));
		$worksheet->write(0, 1, iconv("UTF-8","GBK//IGNORE", "微博昵称"));
		$worksheet->write(0, 2, iconv("UTF-8","GBK//IGNORE", "姓名"));
		$worksheet->write(0, 3, iconv("UTF-8","GBK//IGNORE", "性别"));
		$worksheet->write(0, 4, iconv("UTF-8","GBK//IGNORE", "最后登录省份"));
		$worksheet->write(0, 5, iconv("UTF-8","GBK//IGNORE", "最后登录城市"));
		$worksheet->write(0, 6, iconv("UTF-8","GBK//IGNORE", "邮箱"));
		$worksheet->write(0, 7, iconv("UTF-8","GBK//IGNORE", "手机"));
		$worksheet->write(0, 8, iconv("UTF-8","GBK//IGNORE", "地址"));
		// $worksheet->write(0, 9, iconv("UTF-8","GBK//IGNORE", "温度"));
		$worksheet->write(0, 9, iconv("UTF-8","GBK//IGNORE", "来源"));
		
		// $worksheet->write(0, 11, iconv("UTF-8","GBK//IGNORE", "存温暖"));
		// $worksheet->write(0, 12, iconv("UTF-8","GBK//IGNORE", "求抱抱"));
		// $worksheet->write(0, 13, iconv("UTF-8","GBK//IGNORE", "盼团圆"));
		// $worksheet->write(0, 14, iconv("UTF-8","GBK//IGNORE", "脱光光"));
		// $worksheet->write(0, 15, iconv("UTF-8","GBK//IGNORE", "捐献"));
		// $worksheet->write(0, 16, iconv("UTF-8","GBK//IGNORE", "生成优惠券（消耗300，是/否）"));
		// $worksheet->write(0, 17, iconv("UTF-8","GBK//IGNORE", "上传收据（是/否）"));
		$worksheet->write(0, 10, iconv("UTF-8","GBK//IGNORE", "冰冻术"));
		$worksheet->write(0, 11, iconv("UTF-8","GBK//IGNORE", "解救数"));
		$worksheet->write(0, 12, iconv("UTF-8","GBK//IGNORE", "被动数"));
		$worksheet->write(0, 13, iconv("UTF-8","GBK//IGNORE", "时间"));
        $worksheet->write(0, 14, iconv("UTF-8","GBK//IGNORE", "IP"));
        $worksheet->write(0, 15, iconv("UTF-8","GBK//IGNORE", "PLATFORM"));
		
		$r=1;
		foreach($itemlist  as $key=>$item)
        {
			$worksheet->writeString($r, 0, iconv("UTF-8","GBK//IGNORE",$item["weibo_uid"]));
			$worksheet->writeString($r, 1, iconv("UTF-8","GBK//IGNORE",$item["screen_name"]));
			$worksheet->writeString($r, 2, iconv("UTF-8","GBK//IGNORE",$item["username"]));
			$worksheet->writeString($r, 3, iconv("UTF-8","GBK//IGNORE",$item["gender"]));
			$worksheet->writeString($r, 4, iconv("UTF-8","GBK//IGNORE",$item["last_login_province"]));
			$worksheet->writeString($r, 5, iconv("UTF-8","GBK//IGNORE",$item["last_login_city"]));
			$worksheet->writeString($r, 6, iconv("UTF-8","GBK//IGNORE",$item["email"]));
			$worksheet->writeString($r, 7, iconv("UTF-8","GBK//IGNORE",$item["mobile"]));
			$worksheet->writeString($r, 8, iconv("UTF-8","GBK//IGNORE",$item["address"]));
			// $worksheet->writeString($r, 9, iconv("UTF-8","GBK//IGNORE",$item["mytemp"]));
			$worksheet->writeString($r, 9, iconv("UTF-8","GBK//IGNORE",$item["from"]));

			$worksheet->writeString($r, 10, iconv("UTF-8","GBK//IGNORE",$item["freeze_num"]));
			$worksheet->writeString($r, 11, iconv("UTF-8","GBK//IGNORE",$item["rescue_num"]));
			$worksheet->writeString($r, 12, iconv("UTF-8","GBK//IGNORE",$item["frozen_num"]));
			
			// $worksheet->writeString($r, 11, iconv("UTF-8","GBK//IGNORE",$item["warm"]));
			// $worksheet->writeString($r, 12, iconv("UTF-8","GBK//IGNORE",$item["embrace"]));
			// $worksheet->writeString($r, 13, iconv("UTF-8","GBK//IGNORE",$item["reunion"]));
			// $worksheet->writeString($r, 14, iconv("UTF-8","GBK//IGNORE",$item["tuoguang"]));
			// $worksheet->writeString($r, 15, iconv("UTF-8","GBK//IGNORE",$item["donation"]));
			// $worksheet->writeString($r, 16, iconv("UTF-8","GBK//IGNORE",$item["giftinfo"]?"是":"否"));
			// $worksheet->writeString($r, 17, iconv("UTF-8","GBK//IGNORE",(isset($item["giftinfo"]["receipt"]) && $item["giftinfo"]["receipt"])?"是":"否"));
			
			$worksheet->writeString($r, 13, iconv("UTF-8","GBK//IGNORE",$item["time"]));
            $worksheet->writeString($r, 14, iconv("UTF-8","GBK//IGNORE",$item["ip"]));
            $worksheet->writeString($r, 15, iconv("UTF-8","GBK//IGNORE",$item["platform"]));
        	$r++;
		}
		$workbook->close();
	}
}

?>