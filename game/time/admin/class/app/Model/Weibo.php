<?php
/**
 * Example model
 * Created 2012-11-25 11:24:42
 * @package		classes
 * @subpackage	operator.game
 * @author gary wang (wangbaogang123@hotmail.com)
 * 
 */
class Model_Weibo extends Fuse_Model
{

	/**
	 * array
	 */
	public $table = array("name"=>"weibo_users","key"=>"uid");

	
	public function __construct($config=array())
	{
		parent::__construct($config);
	}
	
	function getCategoryNum($uid, $category){
		$total = 0;
        
		$sql = "SELECT COUNT(*) AS total FROM `user_temp` WHERE `uid`='{$uid}' AND `category`='{$category}'";
		if(($stmt = $this->db->query($sql)))
		{
			if($row = $stmt->fetch())
			{
				$total = $row['total'];
			}
		}
		
		return $total;	
	}

	function getUserDonation($uid){
		$total = 0;
        
		$sql = "SELECT sum(`points`) AS total FROM `donation` WHERE `uid`='{$uid}' ";
		if(($stmt = $this->db->query($sql)))
		{
			if($row = $stmt->fetch())
			{
				$total = $row['total'];
			}
		}
		
		return $total;	
	}
	
	function getUserShouju($uid){
		$item = "";
        
		$sql = "SELECT * FROM `gift` WHERE `uid`='{$uid}'";
		if(($stmt = $this->db->query($sql)))
		{
			if($row = $stmt->fetch())
			{	
				$item = $row;
			}
		}
		
		return $item;		
	}
	
	public function getList($start=0,$per_page=10,$where=1)
	{
		$list = array();
		$sql = " SELECT * FROM `{$this->table['name']}` WHERE {$where} ORDER BY `{$this->table['key']}` DESC";

		if($start>=0 && !empty($per_page)){
			$sql .= " LIMIT {$start},{$per_page}";
		}
		
		if( ($stmt = $this->db->query($sql)) )
		{
			while ( $row = $stmt->fetch() )
			{
				// $row['warm'] = $this->getCategoryNum($row['uid'], 1);
				// $row['embrace'] = $this->getCategoryNum($row['uid'], 2);
				// $row['reunion'] = $this->getCategoryNum($row['uid'], 3);
				// $row['tuoguang'] = $this->getCategoryNum($row['uid'], 4);
				// $row['donation'] = $this->getUserDonation($row['uid']);
				// $row['giftinfo'] = $this->getUserShouju($row['uid']);

				$row['freeze_num'] = $this->getNum($row['weibo_uid'],1);
				$row['rescue_num'] = $this->getNum($row['weibo_uid'],2);
				$row['frozen_num'] = $this->getNum($row['weibo_uid'],3);
				$row['help_num'] = $this->getNum($row['weibo_uid'],4);
				$list[] = $row;
			}
		}
		return $list;
	}


	public function getNum($weibo_uid,$type){
		switch($type){
			case 1:
			$total = $this->getTotal2(" `weibo_uidf`='$weibo_uid'","freeze_list");
			return $total;
			break;
			case 2:
			$total = $this->getTotal2(" `rescue_uid`='$weibo_uid'","rescue_list");
			return $total;
			break;
			case 3:
			$total = $this->getTotal2(" `weibo_uidt`='$weibo_uid'","freeze_list");
			return $total;
			break;
			case 4:
			$total = $this->getTotal2(" `weibo_uidf`='$weibo_uid'","help_list");
			return $total;
			break;
		}
	}

	public function getTotal2($where=1,$table)
	{
		$total = 0;
        
		$sql = "SELECT COUNT(*) AS total FROM `{$table}` WHERE {$where}";
		if(($stmt = $this->db->query($sql)))
		{
			if($row = $stmt->fetch())
			{
				$total = $row['total'];
			}
		}
		
		return $total;
	}

	
	public function getTotal($where=1)
	{
		$total = 0;
        
		$sql = "SELECT COUNT(*) AS total FROM `{$this->table['name']}` WHERE {$where}";
		if(($stmt = $this->db->query($sql)))
		{
			if($row = $stmt->fetch())
			{
				$total = $row['total'];
			}
		}
		
		return $total;
	}
	
	public function getRowOne($id)
	{
		$list = null;
		
		$sql = "SELECT * FROM `{$this->table['name']}` WHERE `{$this->table['key']}`='{$id}'";
		
		if(($stmt = $this->db->query($sql)))
		{
			if($row = $stmt->fetch())
			{
				$list = $row;
			}
		}
		return $list;
	}
 
	public function delete($id)
	{
		$sql = "DELETE FROM `{$this->table['name']}` WHERE `{$this->table['key']}`='{$id}'";
		return $this->db->query($sql);
	}
	
	public function getKey()
	{
		return $this->table['key'];
	}
	
	public function getTable()
	{
		return $this->table['name'];
	}
}
?>