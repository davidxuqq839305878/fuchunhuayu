﻿<?php
/**
 * User: David Xu
 * Date: 2/17/14
 * Time: 3:49 PM
 */
include "./conn.php";
include "./funs.php";
$dateid = getCurrentDateId();//根据不同的DateID来进入不同的活动

$wxid = $_GET['weixin_openid'] ? $_GET['weixin_openid'] :$_GET['wxid'];//微信用户id
$from = $_GET['diyfrom'] ? trim($_GET['diyfrom']):'self';

$awardcount = getFlowerCount($wxid, $dateid);
$userarr = getWeiUserJson($wxid);

if(empty($wxid)){
    echo '{"err"  : "wxid is null"}';
    exit();
}
if($from=='self'){
    //来自self
    //判断此用户是否注册过此活动，也就是判断是否第一次参加本活动
    if(!empty($userarr['data']['name'])){
        $usertype = 1;//此用户是微会员
        $username = $userarr['data']['name'];
        $userphone = $userarr['data']['phone'];

    }else{
        $usertype = 0;//此用户不是微会员
        $username ='';
        $userphone = '';
        //需要引导用户注册信息
    }

    if(existsUserInfo($wxid)){
        //已注册活动信息
        $lasttime = date('Y-m-d H:i:s',time());
        $existsuserinfo = 1;
        if($usertype == 1){
            $updatesql = "UPDATE user_flower_activity SET name = '$username',tel = '$userphone',lasttime = '$lasttime' WHERE wxid ='$wxid'";
            mysql_query($updatesql);//更新用户的手机号码和姓名
        }
    }else{
        //未注册活动信息
        $existsuserinfo = 0;
        $jointime = date('Y-m-d H:i:s',time());
        $lasttime = $jointime;
        if($usertype == 1){
            $exinsertsql = "INSERT INTO user_flower_activity (wxid, name, jointime, lasttime, tel) VALUES('$wxid', '$username', '$jointime', '$lasttime', '$userphone') ";
            mysql_query($exinsertsql);//更新用户的手机号码和姓名
        }else{
            $exinsertsql = "INSERT INTO user_flower_activity (wxid, name, jointime, lasttime, tel) VALUES('$wxid', '', '$jointime', '$lasttime', '') ";
            mysql_query($exinsertsql);//更新用户的手机号码和姓名
        }

    }
    if(existsUserFlowerActivity($wxid, $dateid)){
        //是否参加当前活动
    }else{
        $insertsql = "INSERT INTO activity_info(wxid, dateid, activityarr) VALUES('$wxid','$dateid','')";
        mysql_query($insertsql);
    }
    if(!isset($_COOKIE['random'])){
        setcookie("random", md5($wxid), time()+3600);//如果还没有cookie
        $random = $_COOKIE['random'];
    }else{
        $random = $_COOKIE['random'];//如果有了cookie
    }
}else{
    //来自share
    //setcookie("wxid", "Alex Porter", time()+3600);
    if(isset($_COOKIE['random'])){
       if($_COOKIE['random'] == $_GET['random']){
           //跳转到自己的页面
           header("Location: http://www.elegant-prosper.com/peony/game/index.php?weixin_openid=".$wxid);
           exit;
       }
    }else{
        $random = 0;
    }

}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0,maximum-scale=1, user-scalable=no">                 <!--强制让文档的宽度与设备的宽度保持1:1，并且文档最大的宽度比例是1.0，且不允许用户点击屏幕放大浏览；-->
<meta content="yes" name="apple-mobile-web-app-capable" /> <!--允许全屏模式浏览-->
<meta content="telephone=no" name="format-detection" /> <!--忽略将页面中的数字识别为电话号码-->
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<title>EP雅莹</title>
<link rel="stylesheet" href="css/global.css?date=<?php echo date('Ymdhis',time());?>" type="text/css" />
<link rel="stylesheet" href="css/home.css?date=<?php echo date('Ymdhis',time());?>" type="text/css" />
<script src="js/jquery.min.js"></script>
<script src="js/share.js?date=<?php echo date('Ymdhis',time());?>"></script>
<script src="js/screenRotation.js"></script>
<script src="js/screenSize.js"></script>
<script src="js/funs.js?date=<?php echo date('Ymdhis',time());?>"></script>
<!--David.Xu Script Start-->
<script src="js/load.js?date=<?php echo date('Ymdhis',time());?>"></script>
<!--David.Xu Script End-->
</head>
<body>
<!--David.Xu Html Start-->
<div style="position: fixed;left:0;display:none;">
    微信id:<input type="text" id="wxid" name="wxid" value="<?php echo $wxid;?>"/>
    <br/>来源：<input type="text" id="from" name="from" value="<?php echo $from;?>"/>
    <br/>拥有花语数：<input type="text" id="awardcount" name="awardcount" value="<?php echo $awardcount;?>"/>
    <br/>还可以拥有多少花语；<input type="text" id="flowerlimitcount" name="flowerlimitcount" value="<?php if('err' == strval(getFlowerLimit($wxid, 'self-limit', $from, $dateid))){echo '此用户未参加采集花语活动，非法进入此页面,请先参加活动';}else{echo getFlowerLimit($wxid, 'self-limit', $from, $dateid);}?>"/>
    <br/>点击次数：<input type="text" id="clickcount" name="clickcount" value="0"/>
    <br/>用户类型：<input type="text" id="usertype" name="usertype" value="<?php echo $usertype;?>"/>
    <br/>用户姓名：<input type="text" id="username" name="username" value="<?php echo $username;?>"/>
    <br/>用户手机号码：<input type="text" id="userphone" name="userphone" value="<?php echo $userphone;?>"/>
    <br/>当前dateid：<input type="text" id="dateid" name="dateid" value="<?php echo $dateid;?>"/>
    <br/>是否注册过本活动：<input type="text" id="existsuserinfo" name="existsuserinfo" value="<?php echo $existsuserinfo;?>"/>
    <br/>帮朋友获得花语数量：<input type="text" id="sharecount" name="sharecount" value="0"/>
    <br/>随机数：<input type="text" id="random" name="random" value="<?php echo $random;?>"/>
</div>
<!--David.Xu Html End-->
<section id="section">
    <header class="header"></header>
    <div class="main">
        <!--远山、亭-->
        <div class="an_mountain"><img src="img/animation/an_mountain.png" /></div>
        <div class="an_water"><img src="img/animation/an_water.png" /></div>
        <div class="an_kiosk"><img src="img/animation/an_kiosk.png" /></div>
        <!--花-->
        <div class="an_flower_upperLeft"><img src="img/animation/an_flower_upperLeft.png" /></div>
        <div class="an_flower_left">
            <img src="img/animation/an_flower_left.png" />
            <a id="flower1" href="javascript:void(0)" class="flower_01 flower"><img src="img/animation/flower_01.png" /></a>
            <!-- Modified by David.Xu about id=flower1 class=flower-->
        </div>
        <div class="an_flower_right">
            <img src="img/animation/an_flower_right.png" />
            <!-- Modified by David.Xu about id=flower2 flower3 flower4 flower5 class=flower-->
            <a id="flower2" href="javascript:void(0)" class="flower_02 flower"><img src="img/animation/flower_02.png" /></a>
            <a id="flower3" href="javascript:void(0)" class="flower_03 flower"><img src="img/animation/flower_03.png" /></a>
            <a id="flower4" href="javascript:void(0)" class="flower_04 flower"><img src="img/animation/flower_04.png" /></a>
            <a id="flower5" href="javascript:void(0)" class="flower_05 flower"><img src="img/animation/flower_05.png" /></a>
            <a id="flower6" href="javascript:void(0)" class="flower_06 flower"><img src="img/animation/flower_06.png" /></a>
            <!-- Modified by David.Xu about id=flower2 flower3 flower4 flower5 class=flower-->
        </div>
        <div class="logo"><img src="img/logo.png" /></div>
        <div class="shade"></div>
        <div class="secondShade"></div>
        <div class="tip"><p>Tip: 点击屏幕的牡丹寻找并收集花语</p></div>
        <div class="shadeTop"></div><!--最顶部遮罩层-->
<!--		<div class="menus">-->
<!--            <h2><i>&nbsp;</i> <a href="javascript:void(0)" class="fashionChinaMenus">时尚中国</a></h2>-->
<!--            <ul class="menuVideo">-->
<!--                <li class="fashionVideo fashionVideoA">时尚中国疯</li>-->
<!--                <li class="fashionVideo fashionVideoB">春夏流行不留白</li>-->
<!--                <li class="fashionVideo fashionVideoC">时尚正在耍花样</li>-->
<!--            </ul>-->
<!--            <h2><i>&nbsp;</i>-->
<!--                --><?php //if($from =='self'){?>
<!--                <a href="javascript:void(0)" class="myFlowersMenus">我的花语</a>-->
<!--                --><?php //}else{?>
<!--                <a href="javascript:void(0)" class="joinMenus">我想参与</a>-->
<!--                --><?php //}?>
<!--            </h2>-->
<!--            <div class="line"></div>-->
<!--            <h2><i>&nbsp;</i> <a href="javascript:void(0)" class="rulesMenus">活动规则</a></h2>-->
<!--        </div>-->

		<div class="menus">
            <h2><i>&nbsp;</i><a href="javascript:void(0)" class="fashionChinaMenus">首·页</a></h2>
            <h2><i>&nbsp;</i><a href="#">视·享</a></h2>
            <ul>
                <li>——<a href="javascript:void(0)" class="fashionVideoA">时尚中国疯</a></li>
                <li>——<a href="javascript:void(0)" class="fashionVideoB">春夏流行不留白</a></li>
                <li>——<a href="javascript:void(0)" class="fashionVideoC">时尚正在耍花样</a></li>
            </ul>
            <h2><i>&nbsp;</i><a href="javascript:void(0)">花·语</a></h2>
            <ul><?php if($from =='self'){?>
                <li>——<a style="cursor:pointer" class="myFlowersMenus">我的花语</a></li>
                <?php }else{?>
                <li>——<a  style="cursor:pointer" class="joinMenus">我想参与</a></li>
                <?php }?>
            </ul>
            <h2><i>&nbsp;</i><a href="javascript:void(0)">知·福</a></h2>
            <ul>
                <li>——<a href="javascript:void(0)" class="rulesMenus">活动规则</a></li>
            </ul>
        </div>


    </div>
    <footer class="footer" style="display:none;">
        <a class='fashion'  href="javascript:void(0)">时尚中国</a>
        <em>|</em>
        <?php if($from =='self'){?>
            <a class='myflowers2'  href="javascript:void(0)">我的花语</a>
        <?php }else{?>
            <a class='join'  href="javascript:void(0)">我想参与</a>
        <?php }?>
        <em>|</em>
        <a class='rules'  href="javascript:void(0)">活动规则</a>
    </footer>
    <footer id="nav">
        <div class="btn">
            <i></i><i></i><i></i>
        </div>
    </footer>
    <div class="overbox"></div>
</section>
<div class="backdrop-orientation"></div>
<div class="modal-orientation">
    <img src="img/icon-orientation.png" alt="" />
    <p>竖屏浏览，效果更佳。</p>
    <span class="btn-close ir">关闭</span>
</div>
<!--进入游戏时第一次自动提示试试点击粉色花朵-->
<div class="gatherPopup firstGatherPopup" id="firstGatherPopup" style="display: none;">
    <div class="explain">
        <p class="center"><img src="/peony/images/wenzi.png" style="width:60%;" /></p>
    </div>
</div>
<!--收集花语弹出框-->
<div class="gatherPopup" id="gatherPopup" style="display:none;">
    <img id="gatherPopupImg" src="img/property.png" /><!--需要根据用户的采集花语数量来确定img的SRC-->
    <p><span id="shangShi">落尽残红始吐芳</span>——</p>
    <p>——<span id="xiaShi">佳名唤作百花王</span></p>
    <div class="explain">
        <h2 class="center">恭喜您！</h2>
        <p  class="center">您已经成功收集到<span id="gatherPopupFlower">富贵牡丹</span>花语啦！</p><!--需要根据用户的采集花语数量来确定span的val-->
    </div>
</div>
<!--最后一次显示的页面-->
<!--完善资料以便邮寄奖品-->
<div class="gatherPopup" id="perfectGatherPopup" style="display: none;">
    <div class="explain">
<!--  <p>恭喜您m,已经收集3朵花语！<span id="own">*</span>朵花语，剩余的<span id="surplus">*</span>朵花语需要朋友一起帮忙收集哦！</p>-->
        <p class="center bold">恭喜您，已经收集<span id="own">*</span>朵花语！</p>
<!--        <p>赶快点击右上角分享到朋友圈，让您的好友帮您一起收集花语吧！</p>-->
        <p>分享至朋友圈或好友，求助好友来帮您收集花语吧！6朵以上即有机会获得精美奖品。</p>
        <?php if($usertype == 0){//未注册会员的用户，引导用户注册?>
<!--            <p class="registerGatherDes">在此之前，是否先填写个人资料？没有个人资料将被视为自动放弃获奖资格哦！</p>-->
        <?php }?>
    </div>
    <?php if($usertype == 0){//未注册会员的用户，引导用户注册?>
        <a href="javascript:void(0)" class="perfect_but registerGatherDes">完善资料以便邮寄奖品</a>
        <p class="registerGatherDes">(如未完善，则视您放弃获奖机会)</p>
    <?php }?>
</div>
<!--没有花语-->
<div class="gatherPopup" id="noGatherPopup" style="display: none;">
    <div class="explain">
        <!--p>很遗憾，这朵花没有花语，试试其他花朵吧！</p-->
        <p class="center">没有花语，再试试其他的牡丹吧！</p>
    </div>
</div>

<!--帮朋友找-->
<div class="gatherPopup" id="friendGatherPopup" style="display: none;">
    <img id="friendPopupImg" src="img/property.png" /><!--需要根据用户的采集花语数量来确定img的SRC-->
    <p><span id="shangShi">落尽残红始吐芳</span>——</p>
    <p>——<span id="xiaShi">佳名唤作百花王</span></p>
    <div class="explain">
        <p class="bold">恭喜您，帮好友收集到<span id="friendPopupFlower">****</span>花语！</p>
        <ul>
            <li>关注EP雅莹官方微信，</li>
            <li>采撷花语，同赏大奖。</li>
        </ul>
    </div>
</div>
<!--帮朋友找，点击六次之后的页面-->
<div class="gatherPopup friendGatherLastPopup" id="friendGatherLastPopup" style="display: none;">
    <div class="explain">
        <p class="center"><span id="helper">抱歉，您没有帮您的好友获取到花语，下次再试试吧</span></p><!--需要根据用户的采集花语数量来确定span的val-->
        <p class="center"><img src="img/EPWX.jpg"/> </p>
        <ul>
            <li class="center">也想参与：</li>
            <li class="center">点击右上角----查看公众号</li>
            <li class="center">或查找微信公众号：EP雅莹</li>
            <li class="center">或搜索微信号：epyaying</li>
        </ul>
    </div>
</div>
<!--分享之后，我想参与的页面-->
<div class="gatherPopup" id="friendGatherJoinLastPopup" style="display: none;">
    <div class="explain">
        <p>您可以关注EP雅莹官方微信，参与时尚中国富春花语季活动,赢取精美礼品！</p><!--需要根据用户的采集花语数量来确定span的val-->
        <ul>
            <li>关注方式：</li>
            <li>点击右上角→查看公众号</li>
            <li>或查找微信公众号：EP雅莹</li>
            <li>或搜索微信号： epyaying</li>
        </ul>
    </div>
</div>
<!--完善用户信息-->
<div class="gatherPopup" id="messageGatherPopup" style="display: none;">
    <h2>完善用户信息</h2>
    <form>
        <div class="labText">
            <label><em>*</em>您的姓名</label><input type="text" name="user_name" class="iptText ipTextName" />
        </div>
        <div class="labText">
            <label><em>*</em>移动电话</label><input type="text" name="user_phone" class="iptText ipTextTel" />
        </div>
        <input type="button" class="iptBut" />
    </form>
</div>
<!--时尚中国-->
<div class="fashionChina" id="navContent_01" style="display: none;">
    <div class="w300">
        <a href="javascript:void(0)" class="video_prepare">时尚中国</a>
        <?php if($from =='self'){?>
            <a href="javascript:void(0)" class="immediately">立即开始寻找花语</a>
            <a href="javascript:void(0)" class="immediatelyCheck">查看我的花语</a>
        <?php }else{?>
            <a href="javascript:void(0)" class="immediatelyHelp">帮他寻找花语</a>
        <?php }?>
    </div>
</div>
<!--我的花语-->
<div class="navContent" id="navContent_02" style="display:none;">
    <div class="myFlowers">
        <p class="myP">截止<span class="nowtime">*年*月*日 00:00:00</span>，您一共获得<span class="x">X</span>朵花语</p>
        <ul class="badge">
            <!--需要重新定义的LI-->
        </ul>
    </div>
</div>
<!--活动规则-->
<div class="navContent" id="navContent_03" style="display: none;">
    <div class="acIntroduction">
        <h2>活动介绍</h2>
        <p>时尚中国，富春花语季！<p>
        <p>这个春夏，EP雅莹从江南水乡的灵动中寻找灵感，将刺绣，国画、青花瓷的深层文化底蕴与时尚完美结合,更邀请超模杜鹃一同演绎CHINA CHIC《时尚中国疯》。乍暖还寒时节，不妨和杜鹃一起，踏青寻芳，享春日生活闲趣。参与EP雅莹“时尚中国，富春花语季”互动活动，采撷花语，同赏大奖。<p>
        <p>即日起关注EP雅莹官方微信，参与寻花之旅程，即可感受EP雅莹带来的美好江南风景，及春日生活闲趣，还有机会赢得惊喜好礼，每两周开奖哦！<p>

        <dl>
            <dt class="bold">活动规则</dt><br/>
            <dd>点击活动页面中的牡丹花寻找花语，收集到的花语越多，获得的奖品越丰厚。回复关键词【我的花语】可以查询当期收集的花语数量。</dd>
        </dl>

        <dl>
        <dt class="bold">活动时间</dt><br/>
            <dd>2014年3月10日-2014年4月6日</dd>
            <dd>第1期花语收集：3月10日-3月23日</dd>
            <dd>第2期花语收集：3月24日-4月6日</dd>
        </dl>

        <dt class="bold">奖项设置（每期奖项）</dt><br/>
        <dd class="ddindenttxt">活动期间，每期收集到相应花语数量的参与用户，可获得以下奖品：</dd>
        <dd class="red">一等奖（每期1位，16Gwifi版iPADmini）</dd>
        <dd>收集满8个花语，即可获得大奖，先到先得；</dd>
        <dd class="red">二等奖（每期3位，2000元EP雅莹电子现金券）</dd>
        <dd>收集7及7个以上花语，即有机会获得二等奖；</dd>
        <dd class="red">三等奖（每期10位，价值899元牡丹花型胸针）</dd>
        <dd>收集6及6个以上花语，即有机会获得三等奖；</dd>
        <dd class="red">参与奖（每期500位，价值10元手机话费）</dd>
        <p><img class='prize' src="img/prize.png"/> </p>
        <dl>
            <dt class="bold">活动须知</dt><br/>
            <dd>1. 本次活动最终解释权归EP雅莹所有；</dd>
            <dd>2. 活动期间每两周公布一次获奖名单，请关注EP雅莹官方微博以及活动网站获奖名单；</dd>
            <dd>3. 所有通过刷票或其他不正当行为获奖的用户，将直接取消其帐号参与资格而不另行通知；</dd>
            <dd>4. 每一个用户只能获奖一次，不能重复获奖；</dd>
            <dd>5. 个人收集的花语将于规定下按活动收集周期结束后清空，每期重新计算；</dd>
            <dd>6. 参与奖以周期内第一次分享作为有效分享；</dd>
            <dd>7. 未完善个人获奖信息者视为自动放弃获奖资格；</dd>
            <dd>8. 所有奖品将在活动结束后统一发放；</dd>
            <dd>9. 本次活动中，参与活动的用户将自动视为同意活动细则的有关条款。</dd>
        </dl>


    </div>
</div>
<!--更新成功会员信息提示框-->
<div class="gatherPopup" id="registerRemoteUserInfoOK" style="display: none;">
    <div class="explain">
        <p>注册信息成功，分享至朋友圈或好友，求助好友来帮您收集花语吧！6朵以上即有机会获得精美奖品。</p><!--需要根据用户的采集花语数量来确定span的val-->
    </div>
</div>

<!--更新失败会员信息提示框-->
<div class="gatherPopup" id="registerRemoteUserInfoError" style="display: none;">
    <div class="explain">
        <p>注册信息失败，请稍后再尝试一次。欢迎继续关注EP雅莹官方微信，参与时尚中国富春花语季活动,赢取精美礼品！</p><!--需要根据用户的采集花语数量来确定span的val-->
    </div>
</div>
<!--更新失败会员信息提示框-自定义内容-->
<div class="gatherPopup" id="registerRemoteUserInfoDiy" style="display: none;">
    <div class="explain">
        <p></p><!--需要根据用户的采集花语数量来确定span的val-->
    </div>
</div>
</body>
</html>