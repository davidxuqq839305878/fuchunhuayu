<!--Share Start-->
//记录分享情况
function whoShares(weixin_openid){
    //谁分享过，记录wxid和分享时间
    $.ajax({
        type:'GET',
        url:'http://www.elegant-prosper.com/peony/game/shareStatus.php',
        data:{
            'weixin_openid':weixin_openid,
            'type':'share'//标记为分享记录
        },
        contentType: "application/json; charset=utf-8",
        success:function(data){
            console.log(data);
        },
        error:function(msg){
            console.log('Error whoShares function:'+msg);
        }
    });
}

function whoJoins(weixin_openid){
    //通过分享链接进入，记录wxid和进入时间
    $.ajax({
        type:'GET',
        url:'http://www.elegant-prosper.com/peony/game/shareStatus.php',
        data:{
            'weixin_openid':weixin_openid,
            'type':'join'//标记为参加记录
        },
        contentType: "application/json; charset=utf-8",
        success:function(data){
            console.log(data);
        },
        error:function(msg){
            console.log('Error whoJoins function:'+msg);
        }
    });
}

function share(dataForWeixin){
    WeixinJS = typeof WeixinJS!='undefined' || {};
    WeixinJS.hideOptionMenu = function() {
        document.addEventListener('WeixinJSBridgeReady', function onBridgeReady() {
            if (typeof WeixinJSBridge!='undefined')	WeixinJSBridge.call('hideOptionMenu');
        });
    };
    WeixinJS.hideToolbar = function() {
        document.addEventListener('WeixinJSBridgeReady', function onBridgeReady() {
            if (typeof WeixinJSBridge!='undefined') WeixinJSBridge.call('hideToolbar');
        });
    };

    (function(){
        var onBridgeReady=function(){
            WeixinJSBridge.on('menu:share:appmessage', function(argv){
                (dataForWeixin.callback)();
                WeixinJSBridge.invoke('sendAppMessage',{
                    "appid":dataForWeixin.appId,
                    "img_url":dataForWeixin.MsgImg,
                    "img_width":"120",
                    "img_height":"120",
                    "link":dataForWeixin.url,
                    "desc":dataForWeixin.desc,
                    "title":dataForWeixin.title
                }, function(res){});
            });
            WeixinJSBridge.on('menu:share:timeline', function(argv){
                (dataForWeixin.callback)();

                WeixinJSBridge.invoke('shareTimeline',{
                    "img_url":dataForWeixin.TLImg,
                    "img_width":"120",
                    "img_height":"120",
                    "link":dataForWeixin.url,
                    "desc":dataForWeixin.desc,
                    "title":dataForWeixin.title+'\n'+dataForWeixin.desc
                }, function(res){});
            });
            WeixinJSBridge.on('menu:share:weibo', function(argv){
                (dataForWeixin.callback)();
                WeixinJSBridge.invoke('shareWeibo',{
                    "content":dataForWeixin.title,
                    "url":dataForWeixin.url
                }, function(res){});
            });
            WeixinJSBridge.on('menu:share:facebook', function(argv){
                (dataForWeixin.callback)();
                WeixinJSBridge.invoke('shareFB',{
                    "img_url":dataForWeixin.TLImg,
                    "img_width":"120",
                    "img_height":"120",
                    "link":dataForWeixin.url,
                    "desc":dataForWeixin.desc,
                    "title":dataForWeixin.title
                }, function(res){});
            });
        };
        if(document.addEventListener){
            document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
        }else if(document.attachEvent){
            document.attachEvent('WeixinJSBridgeReady'   , onBridgeReady);
            document.attachEvent('onWeixinJSBridgeReady' , onBridgeReady);
        }else{

        }
    })();
}
<!--Share End-->