/**
 * Created by alee on 2/28/14.
 */

$(document).ready(function(){

    //第一次进入游戏界面，自动提示
    var strUrl = window.location.href;
    var arrUrl = strUrl.split('?');
    var hostUrl = arrUrl[0].replace('/index.php','');
    var startI = Math.ceil(Math.random()*6);
    var awardArr = [];
    var wxid = document.getElementById('wxid').value;
    var from = document.getElementById('from').value;
    var random = document.getElementById('random').value ? document.getElementById('random').value : 0;
    var awardcount = document.getElementById('awardcount').value;
    var flowerLimitCount = 3;//默认可以采集三朵花语
    var dateid = $('#dateid').val();
    var timer;
    var awardFlowerJson ={
                            1:{
                            id:1,
                                name:'端庄',
                                coloururl:hostUrl+'/img/flower/colour/1.gif',
                                grayurl:hostUrl+'/img/flower/gray/1.gif',
                                bigurl:hostUrl+'/img/flower/1.gif',
                                shangShi:'惟有牡丹真国色',
                                xiaShi:'花开时节动京城',
                                des:'惟有牡丹真国色，花开时节动京城。'
                            },
                            2:{
                                id:2,
                                name:'富贵',
                                coloururl:hostUrl+'/img/flower/colour/2.gif',
                                grayurl:hostUrl+'/img/flower/gray/2.gif',
                                bigurl:hostUrl+'/img/flower/2.gif',
                                shangShi:'落尽残红始吐芳',
                                xiaShi:'佳名唤作百花王',
                                des:'落尽残红始吐芳，佳名唤作百花王。'
                            },
                            3:{
                                id:3,
                                name:'瑰丽',
                                coloururl:hostUrl+'/img/flower/colour/3.gif',
                                grayurl:hostUrl+'/img/flower/gray/3.gif',
                                bigurl:hostUrl+'/img/flower/3.gif',
                                shangShi:'浓姿贵彩信奇绝',
                                xiaShi:'杂卉乱花无比方',
                                des:'浓姿贵彩信奇绝，杂卉乱花无比方。'
                            },
                            4:{
                                id:4,
                                name:'静好',
                                coloururl:hostUrl+'/img/flower/colour/4.gif',
                                grayurl:hostUrl+'/img/flower/gray/4.gif',
                                bigurl:hostUrl+'/img/flower/4.gif',
                                shangShi:'有此倾城好颜色',
                                xiaShi:'天教晚发赛诸花',
                                des:'有此倾城好颜色，天教晚发赛诸花。'
                            },
                            5:{
                                id:5,
                                name:'幸福',
                                coloururl:hostUrl+'/img/flower/colour/5.gif',
                                grayurl:hostUrl+'/img/flower/gray/5.gif',
                                bigurl:hostUrl+'/img/flower/5.gif',
                                shangShi:'暮春四月柳发时',
                                xiaShi:'花浓幽香掩映姿',
                                des:'暮春四月柳发时，花浓幽香掩映姿。'
                            },
                            6:{
                                id:6,
                                name:'秀雅',
                                coloururl:hostUrl+'/img/flower/colour/6.gif',
                                grayurl:hostUrl+'/img/flower/gray/6.gif',
                                bigurl:hostUrl+'/img/flower/6.gif',
                                shangShi:'何人不爱牡丹花',
                                xiaShi:'占断城中好物华',
                                des:'何人不爱牡丹花，占断城中好物华。'
                            },
                            7:{
                                id:7,
                                name:'嫣然',
                                coloururl:hostUrl+'/img/flower/colour/7.gif',
                                grayurl:hostUrl+'/img/flower/gray/7.gif',
                                bigurl:hostUrl+'/img/flower/7.gif',
                                shangShi:'疑是洛川神女作',
                                xiaShi:'千娇万态破朝霞',
                                des:'疑是洛川神女作，千娇万态破朝霞。'
                            },
                            8:{
                                id:8,
                                name:'雍容',
                                coloururl:hostUrl+'/img/flower/colour/8.gif',
                                grayurl:hostUrl+'/img/flower/gray/8.gif',
                                bigurl:hostUrl+'/img/flower/8.gif',
                                shangShi:'竟夸天下无双艳',
                                xiaShi:'独占人间第一香',
                                des:'竟夸天下无双艳，独占人间第一香。'
                            }
    };


    var shadeFn = function(){
        //$('.gatherPopup').hide();
        if($('.footer').css('bottom') =='-500px'){
            $('.footer').css('bottom',0);
        }
    }
    //$('.shade').bind('click',shadeLastFn);
    //$('.shade').bind('click',shadeFn);
    var shadeLastFn = function(){

        if((parseInt($('#clickcount').val())==6)&&($('#from').val()=='self')&&(parseInt($('#awardcount').val()) >= 3)){
            //点击完六朵之后的操作

            $('.gatherPopup').hide('fast',function(){});
            var own = $('#awardcount').val();
            var surplus = 8 - parseInt($('#awardcount').val());
            //$('#perfectGatherPopup').show();
            $('.gatherPopup#perfectGatherPopup').find("#own").html(own).end().find('#surplus').html(surplus).end().fadeIn();

        }else if((parseInt($('#clickcount').val())==6)&&($('#from').val()=='share')&&(parseInt($('#awardcount').val())==0)){
            //点击完六朵之后的操作,但是原微信ID用户没有获得花语，可能由于 活动几期 有关系
            $('.gatherPopup').hide('fast',function(){});
            $('#friendGatherLastPopup').fadeIn();


        }else if((parseInt($('#clickcount').val())==6)&&($('#from').val()=='share')){
            //点击完六朵之后的操作

            $('.gatherPopup').hide('fast',function(){});
            var _count = parseInt($('#sharecount').val());
            if(_count > 0){
                //没有帮好友获取到花语
                //恭喜你， 帮好友成功收集到5朵花语！
                _html = "恭喜你,帮好友成功收集到"+_count+"朵花语";
                $('#helper').html(_html);
            }
            awardcount = parseInt(document.getElementById('awardcount').value);
            $('#friendGatherPopup').find('#friendPopupFlower').html(awardFlowerJson[awardcount]['name']);
            $('#friendGatherLastPopup').fadeIn();

        }else{
            //$('.gatherPopup').hide();
        }
    }

    /*产生一个随机数的数组*/
    for(var i = startI; i <= startI + parseInt(document.getElementById("flowerlimitcount").value) -1 ; i++){
            if(i <= 6 ){
                awardArr.push(i);
            }else{
                awardArr.push(i%6);
            }
        }
    awardArr.sort();
    /*六次点击之后，自动提示*/
    var showAfterSixTip = function(){
        shadeLastFn();

    }
    /*设置没有采集成功的花朵的提示设置*/
    for(var j = 1; j <= 6; j++){
        if($.inArray(j,awardArr) == -1){
            //不在随机数组中，也就是被用户点击之后，提示采集不成功，“请尝试采集其他花”
            $("#flower"+j).click(function(){
                //hiddenAll(); //隐藏所有的弹出提示框
                $(this).hide();
                $('.secondShade').show();
                $('#noGatherPopup').fadeIn();
                clearTimeout(timer);
                timer = setTimeout(function(){
                    $('#noGatherPopup').fadeOut();
                    $('.secondShade').hide('normal',function(){
                        showAfterSixTip();
                    });
                },3000);
            });
        }else{
            //忽略掉
        }
    }
    console.log(awardArr);

    function updateData(){
        awardcount = parseInt(document.getElementById('awardcount').value);
        $.ajax({
            type: "GET",
            url:hostUrl+"/getawardflower.php?wxid="+wxid+"&diyfrom="+from+"&awardcount="+awardcount+"&dateid="+dateid,
            success:function(data){
                console.log(data);
            },
            error:function(msg){
                console.log(msg);
            }
        });
    }


    function hiddenAll(){
        //隐藏所有的弹出提示框
        $('.fashionChina').hide();
        $('.navContent').hide();
        $('.gatherPopup').hide();
    }
    for(var j = 0 ; j<= parseInt($("#flowerlimitcount").val()) -1; j++){

        $("#flower"+awardArr[j]).click(function(){

            $(this).hide();//隐藏点击后的花朵
            $('.secondShade').show();
            $.ajax({
                type: "GET",
                url: hostUrl+"/getawardflower.php?wxid="+wxid+"&diyfrom="+from+"&awardcount="+awardcount+"&dateid="+dateid,
                contentType: "application/json; charset=utf-8",
                //dataType: "json",
                success: function (data) {
                    // Play with returned data in JSON format
                    if(data=='getaflower'){
                        //少于三朵花语，可以继续获得
                        if(awardcount < 3){
                            awardcount = parseInt(awardcount) + 1;
                            document.getElementById('awardcount').value = awardcount;
                            /*重写提示框的html信息开始*/
                            console.log(awardFlowerJson[awardcount]['bigurl']);
                            $('#gatherPopup').find('#gatherPopupImg').attr('src',awardFlowerJson[awardcount]['bigurl']);
                            $('#gatherPopup').find('#gatherPopupFlower').html(awardFlowerJson[awardcount]['name']);
                            $('#gatherPopup').find('#shangShi').html(awardFlowerJson[awardcount]['shangShi']);
                            $('#gatherPopup').find('#xiaShi').html(awardFlowerJson[awardcount]['xiaShi']);

                            /*重写提示跨的html信息结束*/
                            if(awardcount  == 3){
                                //等于三朵花语，提示
                                /*恭喜您，你已收集到三朵花语，剩余花语需要朋友帮忙收集。
                                 点击右上角分享到朋友圈，让您的好友帮您一起收集花语吧！
                                 * */
                                /*
                                 *请求更新数据库                            *
                                 * */
                                updateData();//更新数据库，记录前三朵采集的时间
                                //$('#perfectGatherPopup').delay(1000).show();
                                //alert('恭喜您，你已收集到三朵花语，剩余花语需要朋友帮忙收集');
                            }
                        }else{
                            console.log('恭喜您，你已收集到三朵花语，剩余花语需要朋友帮忙收集');
                        }
                    }else if(data == 'err'){
                        console.log('此用户未参加采集花语活动，非法进入此页面');
                    }else if(data =='getaflowerbyshare'){
                        //多于三朵花语，提示
                        /*恭喜您，你已收集到三朵花语，剩余花语需要朋友帮忙收集。
                         点击右上角分享到朋友圈，让您的好友帮您一起收集花语吧！
                         * */
                        awardcount  = parseInt($('#awardcount').val()) + 1;
                        $('#sharecount').val(parseInt($('#sharecount').val()) + 1);
                        $('#awardcount').val(awardcount);
                        $('#friendGatherPopup').find('#friendPopupImg').attr('src',awardFlowerJson[awardcount]['bigurl']);
                        $('#friendGatherPopup').find('#friendPopupFlower').html(awardFlowerJson[awardcount]['name']);
                        $('#friendGatherPopup').find('#shangShi').html(awardFlowerJson[awardcount]['shangShi']);
                        $('#friendGatherPopup').find('#xiaShi').html(awardFlowerJson[awardcount]['xiaShi']);

                        console.log('恭喜您帮助你的朋友获得一朵花语');
                        //$('#friendGatherPopup').show();
                    }else if(data =='notgetaflowerbyshareltthree'){

                        console.log('您的朋友还未获得三朵花语，还不能通过朋友花朵其他花语');

                    }else{
                        console.log(data+"有错误！");
                    }

                    //提示采集成功!
                    if($('#from').val()=='self'){
                        //根据不同情况，提示不同的信息框，来自自己的点击
                        $('.gatherPopup').hide();
                        $('#gatherPopup').fadeIn(1000);
                        clearTimeout(timer);
                        timer = setTimeout(function(){
                            $('#gatherPopup').fadeOut();
                            $('.secondShade').hide('normal',function(){
                                showAfterSixTip(); /*六次点击之后，自动提示*/
                            });
                        },3000);
                    }else{
                        //根据不同情况，提示不同的信息框，来自分享的点击
                        $('.gatherPopup').hide();
                        $('#friendGatherPopup').fadeIn(1000);
                        clearTimeout(timer);
                        timer = setTimeout(function(){
                            $('#friendGatherPopup').fadeOut();
                            $('.secondShade').hide('normal',function(){
                                showAfterSixTip(); /*六次点击之后，自动提示*/
                            });
                        },3000);
                    }
                },
                error: function (msg) {
                    console.log(msg);
                }
            });

            //this.style.display = 'none';
        });
    }
    <!--shareStart-->
    var dataForWeixin={
        appId:"",
        MsgImg:"http://www.elegant-prosper.com/peony/game/img/weixin.jpg",
        TLImg:"http://www.elegant-prosper.com/peony/game/img/weixin.jpg",
        url:hostUrl+'/index.php?diyfrom=share&random='+random+'&weixin_openid='+wxid,
        title:'朋友们快来帮我收集花语',
        desc:'EP雅莹时尚中国富春花语季',
        fakeid:"",
        wxid:wxid,
        callback:function(){
            whoShares(dataForWeixin.wxid);//记录谁分享过
        }
    };
    share(dataForWeixin);
    <!--shareEnd-->
    $('.flower').click(function(){
        $('#clickcount').val(parseInt($('#clickcount').val()) + 1);
    });



    //David .Xu Move here from footer
    var formatTime = function(time){
        var str;
        var arr = time.split('-');
str = arr[0]+'年'+arr[1]+'月'+arr[2]+'日';
        return str;
    }

    var wxid = $('#wxid').val();


    //弹出微会员注册窗口
    $('.perfect_but').click(function(){
        $('#perfectGatherPopup').hide();
        $('#messageGatherPopup').fadeIn(1000);
        $('.tip p').hide();
        $('.footer').css('bottom','-500px');
        $('.shade').unbind('click');
    });
    //底部菜单栏弹出层显示隐藏
    $(".footer").find('.fashion').click(function () {
        $("#navContent_01").show().siblings("div").hide();
        $(".tip p").hide();
        gatherPopup(".w300");
        $(".logo").css("z-index", 500);
    });
    if($('#from').val()!='share'){
        $(".footer").find('.fashion').click();
    }
    $('.footer').find('.join').click(function(){
        //hiddenAll();
        $('.fashionChina').hide();
        $('.navContent').hide();
        $('#friendGatherJoinLastPopup').show();
    });
    $("#navContent_01").find('.immediately').click(function(){
        //hiddenAll();
        $('.fashionChina').hide('normal',function(){
            $('.secondShade').fadeIn('normal',function(){
               $('#firstGatherPopup').fadeIn('normal').delay(5000).fadeOut('normal',function(){$('.secondShade').fadeOut('normal',function(){showAfterSixTip();})});
            });

        });
        $('.navContent').hide();
        $(".tip p").show();
    });
    $(".footer").find(".myflowers2").click(function () {

        //点击我的花语，需要用Ajax请求PHP页面获取获奖花语采集信息
        $.ajax({
            type: "GET",
            url: hostUrl+"/getmyflowerdetails.php?weixin_openid="+wxid+'&dateid='+dateid,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                data = eval(data);
                console.log(data);
                var html = '';
                /*
                 * <li>
                 <img src="img/property.png" />
                 <article>
                 <h3>富贵</h3>
                 <p>AT2014年1月20日通过自己收集</p>
                 </article>
                 </li>
                 * */
                if(data.length == 1){
                    var html;

                        for(var i = 0;i <= 7; i++){

                            if(i<=2){
                                html +='<li>';
                                var j = i + 1;
                                html +='<img src="img/flower/gray/'+j+'.gif" />';
                                html +=    '<article>';
                                html +=    '<h3>'+awardFlowerJson[j]['name']+'</h3>';
                                html +=    '<p>自己参与活动收集</p>';
                                html +='</article>';
                                html +='</li>';

                            }else{
                                html +='<li>';
                                var j = i + 1;
                                html +='<img src="img/flower/gray/'+j+'.gif" />';
                                html +=    '<article>';
                                html +=    '<h3>'+awardFlowerJson[j]['name']+'</h3>';
                                html +=    '<p>分享活动至朋友圈邀请好友帮忙</p>';
                                html +='</article>';
                                html +='</li>';

                            }

                        }

                    }else if(data.length == 3){

                        var time = '';
                        for(var i = 0;i <= 7; i++){

                            var j = i + 1;

                            if(i <= 2){

                            var j = i + 1;
                            time = data[i].time;
                            time = formatTime(time.substr(0, 10));
                            html +='<li><img src="'+awardFlowerJson[j]['coloururl']+'" \/>';
                            html +='<article>';
                            html +=    '<h3>'+awardFlowerJson[j]['name']+'</h3>';
                            html +='<p>';
                            html += time;
                            html += '通过自己收集</p>';
                            html +='</li>';

                            }else{
                            html +='<li>';
                            html +='<li><img src="'+awardFlowerJson[j]['grayurl']+'" \/>';
                            html +=    '<article>';
                            html +=    '<h3>'+awardFlowerJson[j]['name']+'</h3>';
                            html +=    '<p>分享活动至朋友圈邀请好友帮忙</p>';
                            html +='</article>';
                            html +='</li>';
                            }
                        }
                    }else{

                        for(var i = 0;i <= 7; i++){

                        var j = i + 1;
                        if(i <= 2){

                            time = data[i].time;
                            time = formatTime(time.substr(0, 10));
                            html +='<li><img src="'+awardFlowerJson[j]['coloururl']+'" \/>';
                            html +='<article>';
                            html +=    '<h3>'+awardFlowerJson[j]['name']+'</h3>';
                            html +='<p>';
                            html += time;
                            html += '通过自己收集</p>';
                            html +='</article>';
                            html +='</li>';
                        }else if(i+1 <= data.length){

                            time = data[i].time;
                            time = formatTime(time.substr(0, 10));
                            html +='<li><img src="'+awardFlowerJson[j]['coloururl']+'" \/>';
                            html +='<article>';
                            html +=    '<h3>'+awardFlowerJson[j]['name']+'</h3>';
                            html +='<p>';
                            html += time;
                            html += '通过朋友收集</p>';
                            html +='</article>';
                            html +='</li>';
                            }else{
                            html +='<li>'
                            html +='<li><img src="'+awardFlowerJson[j]['grayurl']+'" \/>';
                            html +=    '<article>';
                            html +=    '<h3>'+awardFlowerJson[j]['name']+'</h3>';
                            html +=    '<p>分享活动至朋友圈邀请好友帮忙</p>';
                            html +='</article>';
                            html +='</li>';

                            }
                        }
                    }

                var date = new Date();//定义当前日期
                var dateStr = date.getFullYear()+'年'+(date.getMonth()+1)+'月'+date.getDate()+'日'+' '+date.getHours()+':'+date.getMinutes()+':'+date.getSeconds();//定义当前日期
                $('.myP .nowtime').html(dateStr);//定义当前日期
                data.turnlength = data.length == 1 ? 0 : data.length;//定义花语个数
                $('.myP .x').html(data.turnlength);//定义花语个数
                $('#navContent_02 ul.badge').html(html);
                $("#navContent_02").show().siblings("div").hide();
                $(".tip p").hide();
                $(".logo").css("z-index", 200);
            },
            error: function (msg) {
                console.log(msg);//输出错误
            }
        });

    });
    //底部 点击 活动规则
    $(".footer").find(".rules").click(function () {
        $("#navContent_03").show().siblings("div").hide();
        $(".tip p").hide();
        $(".logo").css("z-index", 200);
    });

    //弹出框居中
    //gatherPopup(".menus");
    gatherPopup('.gatherPopup');
    gatherPopup('#firstGatherPopup');
    gatherPopup("#gatherPopup");
    gatherPopup("#noGatherPopup");
    gatherPopup("#friendGatherPopup");
    gatherPopup("#friendGatherLastPopup");
    gatherPopup("#perfectGatherPopup");
    gatherPopup("#messageGatherPopup");

    function gatherPopup(gather) {

        var gatherPopup = $(gather).height() / 2;
        if(gather !='#gatherPopup'){
            $(gather).css("margin-top", -gatherPopup +20 );
        }else{
            $(gather).css("margin-top", -gatherPopup -20 );
        }

//        if( gather !='#gatherPopup'){
//            $(gather).css("margin-top", -gatherPopup +20 );
//        }else if(gather =='#friendGatherLastPopup'){
//            $(gather).css("margin-top", $(gather).height() );
//        }else{
//            $(gather).css("margin-top", -gatherPopup -20 );
//        }
    }

    $(".video_prepare").click(function () {
       // $(".btn").click();
        if($('video').length > 0){
            $('video').remove();
        }
        $("body").append("<video controls='controls' autoplay='autoplay' autobuffer><source src='img/video.mp4'/><source src='img/video2.mp4'/>您的浏览器不支持 video 标签。</video>");
    });
    $(".fashionVideoA").click(function () {
        $(".btn").click();
        if($('video').length > 0){
            $('video').remove();
        }
        $("body").append("<video controls='controls' autoplay='autoplay' autobuffer><source src='/peony/v/video1.mp4'/>您的浏览器不支持 video 标签。</video>");
    });
    $(".fashionVideoB").click(function () {
        $(".btn").click();
        if($('video').length > 0){
            $('video').remove();
        }
        $("body").append("<video controls='controls' autoplay='autoplay' autobuffer><source src='/peony/v/video2.mp4'/>您的浏览器不支持 video 标签。</video>");
    });
    $(".fashionVideoC").click(function () {
        $(".btn").click();
        if($('video').length > 0){
            $('video').remove();
        }
        $("body").append("<video controls='controls' autoplay='autoplay' autobuffer><source src='/peony/v/video3.mp4'/>您的浏览器不支持 video 标签。</video>");
    });


    /*******注册与更新用户的信息*******/
    $('.iptBut').click(function(){
        $('.secondShade').fadeIn();
        var wxid = $('#wxid').val();
        var name = $('.ipTextName').val();
        var tel = $('.ipTextTel').val();
        var remote_url = hostUrl+'/updateRemoteUserInfo.php';
        if(name ==''){
            alert('姓名不要为空哦');
            $('.secondShade').fadeOut();
            return false;
        }
        var reg = new RegExp('^1[3458][0-9]{9}$');
        if(!reg.test(tel)){
            alert('请输入有效的电话号码，否则有可能领不到奖品哦');
            $('.secondShade').fadeOut();
            return false;
        }
        registerRemoteUserInfo(hostUrl, remote_url, wxid, name, tel);
    });

    /**ADD**/
    //底部菜单栏弹出层显示隐藏

    var H =  $(window).height();
    var h =  $(".menus").height();
    $(".menus").css("top",(H-h)/2);



    var timerHome = setTimeout(function(){
        $(".menus").toggleClass("a-fadeoutL");
        $(".overbox").toggleClass("a-fadeoutL");
        if(parseInt($('#usertype').val()) == 1 && parseInt($('#awardcount').val()) >= 3){
            $('.immediately').hide('fast');
            $('.immediatelyCheck').css('display','block');
        }
        //fashionChina
        if($('#from').val() == 'share'){
            $(".footer").find('.fashion').click();
        }
    }, 5000);

//     if($('#from').val() == 'share'){
//         clearTimeout(timerHome);
//         $(".menus").toggleClass("a-fadeoutL");
//         $(".overbox").toggleClass("a-fadeoutL");
//
//     }

    $(".btn").click(function (){
        clearTimeout(timerHome);
        $(".menus").toggleClass("a-fadeoutL").toggleClass("a-fadeinL");
        $(".overbox").toggleClass("a-fadeoutL").toggleClass("a-fadeinL");
        $(".gatherPopup").hide();
        //$('.shade').bind('click',shadeFn);
        //$('.shade').bind('click',shadeLastFn);
        if($('video').length > 0){
            $('video').remove();
        }
        if(parseInt($('#usertype').val()) == 1 && parseInt($('#awardcount').val()) >= 3){
            $('.immediately').hide('fast');
            $('.immediatelyCheck').css('display','block');
        }

    });


//    $(".btn").click(function (){
//        $(".menus").toggleClass("a-fadeinL").toggleClass("a-fadeoutL");
//        $(".overbox").toggleClass("a-fadeinL").toggleClass("a-fadeoutL");
//        setTimeout(hidden,800)
//        $('.shade').bind('click',shadeFn);
//        $('.shade').bind('click',shadeLastFn);
//        if($('video').length > 0){
//            $('video').remove();
//        }
//    });
//
//    function hidden(){
//        var H =  $(window).height();
//        var h =  $(".menus").height();
//        $(".menus").css("top",(H-h)/2);
//        $(".menus").toggleClass("hidden");
//        $(".overbox").toggleClass("hidden");
//    }

    $('.fashionChinaMenus').click(function(){
        $(".btn").click();
        $(".footer").find('.fashion').click();
        if(parseInt($('#usertype').val()) == 1 && parseInt($('#awardcount').val()) >= 3){
            $('.immediately').hide('fast');
            $('.immediatelyCheck').css('display','block');
        }
    });
    $('.immediatelyCheck').click(function(){
        $(".footer").find(".myflowers2").click();
    });
    $('.myFlowersMenus').click(function(){
        $(".btn").click();
        $(".footer").find(".myflowers2").click();
    });
    $('.joinMenus').click(function(){
        $(".btn").click();
        $('.footer').find('.join').click();
    });//immediatelyHelp
    $('.rulesMenus').click(function(){
        $(".btn").click();
        $(".footer").find(".rules").click();
    });
    $('.immediatelyHelp').click(function(){

        $("#navContent_01").hide();
        $('.secondShade').show('normal',function(){
            $('#firstGatherPopup').fadeIn('normal').delay(5000).fadeOut('normal',function(){$('.secondShade').fadeOut('normal',function(){showAfterSixTip();})});
        });
    });
    /*统计数据*/
    if($('#from').val()=='share'){
        whoJoins(wxid);
    }
});