window.onload = function () {
    screenSize();
};
window.onresize = function () {
    screenSize();
};
function screenSize() {
    var winWidth = $(window).width();
    var winHeight = $(document).height();

    $(".main,.navContent,.fashionChina").css("min-height", winHeight);

    var winWidthMain = $(".main").width();
    var winHieghtMain = $(".main").height();

    var heigMountain = $(".an_mountain").height() / 2;
    var heigMoun = $(".an_mountain").height() / 6;
    $(".an_water").css("top", heigMountain + heigMoun);
    var heigKiosk = $(".an_mountain").height() / 9;
    $(".an_kiosk").css("top", heigMountain - heigKiosk);
    var heigFlower_right = $(".an_mountain").height() / 5;
    $(".an_flower_right").css("top", heigMountain + heigFlower_right);


    //花朵--1
    var heigFlower_left = $(".an_flower_left").height() / 3.4;
    var widFlower = winWidthMain / 6;
    var widFlower_left = winWidthMain / 9;
    $(".flower_01").css({ "top": heigFlower_left, "width": widFlower, "left": widFlower_left });

    //花朵--2
    var heigFlowerRight_two = winHieghtMain / 70;
    var widFlower_two = winWidthMain / 5;
    var widgFlowerRight = winWidthMain / 2;
    $(".flower_02").css({ "top": -heigFlowerRight_two, "width": widFlower_two, "left": widgFlowerRight });

    //花朵--3
    var heigFlowerRight_three = $(".an_flower_left").height() / 2.8;
    var widFlower_three = winWidthMain / 5;
    var widgFlowerRight_three = winWidthMain / 4.6;
    $(".flower_03").css({ "top": heigFlowerRight_three, "width": widFlower_three, "left": widgFlowerRight_three });

    //花朵--4
    var heigFlowerRight_four = $(".an_flower_left").height() / 2.6;
    var widFlower_four = winWidthMain / 5;
    var widgFlowerRight_four = winWidthMain / 1.8;
    $(".flower_04").css({ "top": heigFlowerRight_four, "width": widFlower_four, "left": widgFlowerRight_four });

    //花朵--5
    var heigFlowerRight_five = $(".an_flower_left").height() / 1.6;
    var widFlower_five = winWidthMain / 6;
    var widgFlowerRight_five = winWidthMain / 2.6;
    $(".flower_05").css({ "top": heigFlowerRight_five, "width": widFlower_five, "left": widgFlowerRight_five });

    //花朵--6
    var heigFlowerRight_six = $(".an_flower_left").height() / 1.3;
    var widFlower_six = winWidthMain / 5;
    var widgFlowerRight_six = winWidthMain / 2.6;
    $(".flower_06").css({ "top": heigFlowerRight_six, "width": widFlower_six, "left": widgFlowerRight_six });

    $(".navContent").css("width", winWidthMain );
    $(".fashionChina").css("width", winWidth);
    var navWidth = $(".navContent").width() / 2;
    $(".navContent").css("margin-left", -navWidth);

    var navWidthChina = $(".fashionChina").width() / 2;
    $(".fashionChina").css("margin-left", -navWidthChina);
};

window.setTimeout(function () {
    $(".an_flower_upperLeft").css({ "-webkit-animation": "fadeInLeft 3.5s 1 0s", "opacity": 1 });
    $(".an_flower_left").css({ "-webkit-animation": "fadeInUp 3.5s 1 0s", "opacity": 1 });
    $(".an_flower_right").css({ "-webkit-animation": "fadeInRight 3.5s 1 0s", "opacity": 1 });
}, 2000);

window.setTimeout(function () {
    $(".shade").show();
}, 6000);
