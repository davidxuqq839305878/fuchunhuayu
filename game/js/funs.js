/**
 * Created by DavidXu on 2/27/14.
 */

function sleep(n) {
    var start = new Date().getTime();
    while(true)  if(new Date().getTime()-start > n) break;
}
function showMessage(txt){
     $('#registerRemoteUserInfoDiy').find('p').html(txt).end().fadeIn('normal',function(){}).delay(3000).fadeOut('normal',function(){
         $('.secondShade').fadeOut();//掩藏遮罩
     });
}
function registerRemoteUserInfo(hostUrl, remote_url, weixin_openid, user_name, user_phone){
    $.ajax({
        async: false,
        type:'POST',
        url:remote_url,
        data:{
            'weixin_openid':weixin_openid,
            'user_name':user_name,
            'user_phone':user_phone
        },
        dataType:'json',
        success:function(data){
            console.log(data);
            if(data.result != 1){
                showMessage(data.error_message);//提示错误信息
                //请关注EP雅莹官方微信，以进一步完善个人信息。
            }else{
                $('.secondShade').fadeOut();//掩藏遮罩
                $('#messageGatherPopup').fadeOut('normal',function(){
                    //$('#registerRemoteUserInfoOK').fadeIn().delay(3000).fadeOut(function(){});//提示更新成功
                    $('#registerRemoteUserInfoOK').fadeIn();
                    $('#existsuserinfo').val(1);
                    $('.registerGatherDes').hide();
                    $('.shade').bind('click',shadeFn).bind('click',shadeLastFn).click();

                });//隐藏注册狂
                var localUrl = hostUrl+'/updateUserInfo.php';
                registerLocalUserInfo(localUrl, weixin_openid, user_name, user_phone);
            }
        }
    });
}
function registerLocalUserInfo(local_url, weixin_openid, user_name, user_phone){
    $.ajax({
        type:'POST',
        url: local_url,
        async : false,
        data:{
            'weixin_openid':weixin_openid,
            'user_name':user_name,
            'user_phone':user_phone
        },
        dataType:'text',
        success:function(data){

            if(data != 'insertok'){
                $('#registerRemoteUserInfoError').show();
            }else{

            }
        },
        error:function(msg){
            console.log('Error registerLocalUserInfo function:'+msg);
        }
    });
}
function getAwardInfo(weixin_openid, event_id, prize_id){
    $.ajax({
        type:'POST',
        url:'http://why.elegant-prosper.com/?ep_api=event_winning',
        data:{
            'weixin_openid':weixin_openid,
            'event_id':event_id,
            'prize_id':prize_id
        },
        dataType:'json',
        contentType: "application/json; charset=utf-8",
        success:function(data){
            return data;
        },
        error:function(msg){
            console.log('Error getAwardInfo function:'+msg);
        }

    });
}
function killerrors(){
    return true;//忽略JS错误
}
//window.onerror = killerrors// ;
