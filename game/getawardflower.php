<?php
/**
 * User: David Xu
 * Date: 2/18/14
 * Time: 10:07 AM
 */
include "./conn.php";
include "./funs.php";

// opXN8uDV7ZhcGJc-VtSUmQbnkiH4;
$wxid = $_GET['wxid'];
$from = $_GET['diyfrom'] ? $_GET['diyfrom'] : "self";
$awardcount = $_GET['awardcount'] ? $_GET['awardcount'] : 0;
$dateid = $_GET['dateid'];

if(empty($wxid)){
    echo  "微信ID是空的，请传入微信ID的数值";
    exit();
}

if($from == "self"){
    //如果是自己点击进入游戏界面，先判断此用户是否参加过“采集花语活动”
    if(existsUserFlowerActivity($wxid, $dateid)){
        //此用户已经参加此活动，然后判断拥有几朵花语
        $act_data = @unserialize(getInfoUserFlowerActivity($wxid, $dateid));//获取活动情况
        // implode() ---把数组转换成字符串 explode() ---把字符串转换成数组
        // serialize unserialize 函数
        if(empty($act_data)){
            //如果参加活动信息为空，也就是采集的花语少于三朵
            if($awardcount == 3){
                updateThreeUserFlowerActivity($wxid, $dateid);
                echo  'donotgetaflower';
            }else{
                echo  'getaflower';
            }
        }else{
            //如果参加活动不为空，也就是采集的花语多于或者等于三朵，也就是只能通过朋友分享
            echo  'donotgetaflower';
        }
    }else{
        echo  "err";//此用户未参加采集花语活动，非法进入此页面
    }
}

if($from == "share"){
    //如果是别人分享点击进入游戏界面，先判断此用户是否参加过“采集花语活动”
    if(existsUserFlowerActivity($wxid ,$dateid)){
        //此用户已经参加此活动，然后判断拥有几朵花语
        $act_data = @unserialize(getInfoUserFlowerActivity($wxid, $dateid));//获取活动情况
        $flowerCount = getFlowerCount($wxid, $dateid);
        if($flowerCount > 8){
            echo 'overflowerlimit';
            //已经获得大于，这种情况不可能发生
        }else if($flowerCount == 8){
            echo 'equalflowermost';
            //已经获得八朵花语
        }else if($flowerCount == 0){
            echo "notgetaflowerbyshareltthree";
        }else{
            //小于八朵花语
            //然后判断 本次分享点击是否能获得多少朵花语
            $num = getAwardFlowerCount($wxid,$dateid);
            if($num != 0){
                $str = updateGtThreeUserFlowerActivity($wxid,$dateid);
                $userFlowerCount = getFlowerCount($wxid,$dateid);
                echo 'getaflowerbyshare';
            }else{
                echo "donotgetflower";
            }

        }

    }else{
        echo  "err";//此用户未参加采集花语活动，非法进入此页面
    }
    //echo "share";
}
